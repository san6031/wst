var screenIndex = -1;
var clickSound, slideSound;
var appVersion = 1;
var WebTVBrowseId = 0;
var gLevels = ['','','',''];
var logTimet = 0;
function LoadAudio()
{
    Device_LoadAudio();
}

function GetFromPotionK(key)
{
    var parts = Hex2Str(potionK).split('~');
    if (key=='domain_id') return parts[0];
    if (key=='device_id') return parts[1];
    if (key=='member_id') return parts[2];
    if (key=='connection_id') return parts[3];
    if (key=='secret') return parts[4];
    if (key=='device_unique_id') return parts[5];
    return '';
}

function GetPotionK()
{
    var d = new Date();
    var n = d.getTimezoneOffset();
    //console.log('gmtdiff=' + n);
    var k = '&potionK=' + potionK + '&size=' + $(window).width() + ',' + $(window).height() + '&gmtdiff=' + n + '&appVersion=' + appVersion;
    return k;
}

function LogTime(msg)
{
    var d = new Date();
    var n = d.getTime();
    if (logTimet > 0) DeviceConsole('['+msg+'] milliseconds: '+n+', difference: '+ (n - logTimet));
    else DeviceConsole('['+msg+'] milliseconds: '+n);
    logTimet = n;
}

function Debug(str)
{
    if (DeveloperIP()==1) {
        var obj = document.getElementById('debugger');
        if (obj == null) return 0;
        obj.style.display='block';
        obj.innerHTML += str;
    }
}

function ExitApp()
{
    //DeviceConsole('ExitApp : '+typeof(Device_ExitApp));
    if (typeof(Device_ExitApp)=='function') Device_ExitApp();
}

function Loader(what)
{
    if (what=='show') $('#spin_loader').show();
    else if (what=='hide') $('#spin_loader').hide();
}

function GetLastScreen()
{
    var fcon = document.getElementById('fcon');
    if (fcon.childNodes.length==0) return null;

    var i = fcon.childNodes.length-1;
    return fcon.childNodes[i];
}

function SetFocusToLastScreen(setFocusFlag)
{
    //DeviceConsole('SetFocusToLastScreen');

    if (setFocusFlag==undefined) setFocusFlag=1;
    var lastScreen = GetLastScreen();
    if (lastScreen==null) return 0;

    SetActiveScreenIndex(lastScreen.id);
    lastScreen.style.display='block';
    if (lastScreen.tagName=='IFRAME') lastScreen.contentWindow.SetFocus();
    else if (setFocusFlag==1) SetFocus();
    return 1;
}

function PopLastScreen(setFocusFlag, mvc)
{
    //DeviceConsole('PopLastScreen');
    var minViewCount=1;
    if (mvc != undefined) minViewCount=mvc;

    var fcon = document.getElementById('fcon');
    if (fcon.childNodes.length > minViewCount) {
        var lastScreen = fcon.childNodes[fcon.childNodes.length-1];
        RemoveJsCssCode('js', lastScreen.id);
        fcon.removeChild(lastScreen);
    }

    SetFocusToLastScreen(setFocusFlag);
    return fcon.childNodes.length;
}

function RemoveLevel(lvl)
{
    var fcon = document.getElementById('fcon');
    for (var i = 0 ; i < fcon.childNodes.length ; i++) {
        var screen = fcon.childNodes[i];
        if (screen.getAttribute('level')==lvl) {
            fcon.removeChild(screen);
            i--;
        }
    }
}

function GetNextLevel(lvl)
{
    var fcon = document.getElementById('fcon');
    for (var i = 0 ; i < fcon.childNodes.length ; i++) {
        var screen = fcon.childNodes[i];
        if (screen.getAttribute('level') > lvl) return screen;
    }
    return null;
}

function GotoHomeScreen()
{
    var fcon=document.getElementById('fcon');
    var screen=fcon.childNodes[0];
    var lvl=screen.getAttribute('level');
    var ss=1;
    var obj=null;
    while(ss==1) 
    {
        obj = GetNextLevel(lvl);
        if (obj==null) break;
        lvl = obj.getAttribute('level');
        ss = obj.children[1].getAttribute('ss');
    }

    if (obj != null) 
    {
        while (-1) {
            obj = GetNextLevel(lvl);
            RemoveLevel(lvl);

            if (obj==null) break;
            lvl = obj.getAttribute('level');
        }
    }

    SetActiveScreenIndex(fcon.childNodes[0].id);
    SetFocus();
}

function StartApp(htmlJs_status, htmlJs_url)
{
    if (htmlJs_status=='login') WebTVForm(htmlJs_url, 'active', 0);
    else if (htmlJs_status=='expired') WebTVForm(htmlJs_url, 'active', 0);
    else WebTVBrowse(htmlJs_url, '0', 'active');
}

function CloseBanner()
{
    var fcon = document.getElementById('fcon');
    if (fcon.children[0].id == 'banner')
    {
        var htmlJs_status = fcon.children[0].getAttribute('status');
        var htmlJs_url = fcon.children[0].getAttribute('url');
        //DeviceConsole('CloseBanner (removeChild): '+fcon.children[0].id);  
        fcon.removeChild(fcon.children[0]);
        StartApp(htmlJs_status, htmlJs_url);
    }
}

function BannerTimer(toutSecond)
{
    //DeviceConsole('BannerTimer: '+toutSecond);  
    var t = Double(toutSecond);
    if (t > 0) window.setTimeout('CloseBanner()', t*1000);
    SetFocusToInfocus();
}

function LaunchBanner(launch_banner, htmlJs_status, htmlJs_url)
{
    var parts = launch_banner.split('~');
    var imgUrl = WEBROOT + "system/1.0/data/images/banner/" + parts[0];
    var html = "<img src='" + imgUrl + "' style='width:100vw;height:100vh;' onload='BannerTimer(" + parts[1] + ")'>"; 

    //DeviceConsole('LaunchBanner: '+imgUrl);  
    var fcon = document.getElementById('fcon');
    var div = CreateDivView('banner', 'form', '');
    div.setAttribute('status', htmlJs_status);
    div.setAttribute('url', htmlJs_url);
    div.innerHTML = html;

    document.getElementById('fcon').appendChild(div);
    //if (Double(parts[1]) > 0) window.setTimeout('CloseBanner()', Double(parts[1])*1000);
    SetFocusToInfocus();
}


function OnConnected(msg)
{
    //DeviceConsole('OnConnected: '); 

    var d = new Date();
    var n = d.getTimezoneOffset();
    //console.log('timezone offset: '+n+', current time: '+ d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds());
    htmlJs = JSON.parse(msg);
    //DeviceConsole(htmlJs); 
    potionK = htmlJs.potionK;
    var vrObj = document.getElementById('vr');
    if (vrObj.innerHTML == '') vrObj.innerHTML = htmlJs.vremote;

    if (htmlJs.launch_banner != '') { LaunchBanner(htmlJs.launch_banner, htmlJs.status, htmlJs.url); return; }
    StartApp(htmlJs.status, htmlJs.url);
}

function IsFullScreen(vp1)
{
    var x0 = $(window).width() - $(vp1).width();
    //DeviceConsole('IsFullScreen: '+$(window).width()+' - '+$(vp1).width()+' = '+x0);
    if (x0 > 1) return 0;

    x0 = $(window).height() - $(vp1).height();
    //DeviceConsole('IsFullScreen: '+$(window).height()+' - '+$(vp1).height()+' = '+x0);
    if (x0 > 1) return 0;

    return 1;
}

function IsVideoPlaying()
{
    var fcon = document.getElementById('fcon');
    DeviceConsole('IsVideoPlaying: '+fcon.getAttribute('action'));
    if (fcon.getAttribute('action')=='video') return 1;
    return 0;
}

function IsValidTimezone()
{
    var vp1 = GetInputElementByEType('serverlist', 0);
    if (vp1 != null) {
        var ro = Int($(vp1).attr('ro'));
        var itemObj = vp1.children[0].children[ro];
        if (itemObj.getAttribute('ctype')=='video') return 1;
    }
    return 0;
}

function CanGoBack()
{
    if (IsVideoPlaying()==1) return 1;
    var fcon = document.getElementById('fcon');
    if (fcon.childNodes.length > 1) return 1;
    return 0;
}

function SetFocusToInfocus()
{
    //DeviceConsole('SetFocusToInfocus');
    document.getElementById('Infocus').focus();
}

function PlayerStarted(setfocus)
{
    var fcon = document.getElementById('fcon');
    fcon.setAttribute('action', 'video');
    fcon.setAttribute('state', 'play');
    if (setfocus==1) SetFocusToInfocus();
}

function DummyFunction(msg)
{
}

function UpdateVideoPosition(videoPos)
{
    var fcon = document.getElementById('fcon');
    //DeviceConsole('UpdateVideoPosition: ');

    var playerData = fcon.getAttribute('videoData').split('^^');
    fcon.setAttribute('action', '');
    fcon.setAttribute('state', '');
    fcon.setAttribute('videoData', '^^^^^^^^^^^^^^^^');

    var vp1 = GetInputElementByEType('video', 0);
    // This video does not want to save position for resume
    if ($(vp1).attr('nosavepos')==0) 
    {  
        AjaxCall('&action=saveVideoPos&vid=' + playerData[3] + '&pos=' + videoPos + '&tz=' + playerData[5] + GetPotionK(), DummyFunction, '', DeviceConsole);

        var serverlist = GetInputElementByEType('serverlist', 0);
        if (serverlist != null)
        {
            var resumeOption = $(serverlist).find("[ctype='resume']");
            if(resumeOption.length > 0) resumeOption.show();
        }
    }
}

function CloseVideo__(m, videoPos)
{
    DeviceConsole("CloseVideo__("+m+", "+videoPos+")");

    var fcon = document.getElementById('fcon');
    var vp1 = GetInputElementByEType('video', 0);
    if (vp1==null) {
        var vp10 = document.getElementById('videopanel1');
        if (vp10 != null) vp1 = vp10.parentElement;
    }
    if (vp1 != null) {
        //var vp1 = vp.parentElement;
        var playerMode = vp1.getAttribute('mode');

        var videoScreen = fcon.childNodes[fcon.childNodes.length-1];
        if (videoScreen.children.length==1 && videoScreen.children[0].id=='videopanel1') {
            fcon.removeChild(videoScreen);
        }
        else $('#videopanel1').hide();

        UpdateVideoPosition(videoPos);
    }
    //DeviceConsole([m, playerMode]);
    if (m=='back') {
        if (playerMode=='1' && appVersion >= 112) {
            PopLastScreen(1,1);
        }
        else SetFocusToLastScreen(1);
    }
    else if (m=='home') GotoHomeScreen();
}

function CloseVideo118(m)
{
    DeviceConsole('CloseVideo118: '+m);
    // videoPos is set as -1 while called by parent application 

    if (m=='back') PopLastScreen(1,1);
    else if (m=='home') GotoHomeScreen();
    else if (m=='none') {}
    else if (m=='parent') GoBack(m);
    else SetFocusToLastScreen(1);
}

function CloseVideo(m)
{
    DeviceConsole('CloseVideo');
    Device_StopVideo(m);
}

function WebtvShowVideoPlayerAndPlay(playerData)
{
    //DeviceConsole('WebtvShowVideoPlayerAndPlay'+', '+playerData[0]+', '+playerData[1]+', '+playerData[2]+', '+playerData[3]+', '+playerData[4]+', '+playerData[5]+', '+playerData[6]+', '+playerData[7]+', '+playerData[8]);

    var vpObj = document.getElementById('videopanel1');
    if (vpObj == null) {
        vpObj = GetInputElementByEType('video');   
        if (vpObj == null) return;
    }

    $(vpObj).show();
    Device_SetPlayer(playerData[0], playerData[1], playerData[4], 'main_player', playerData[7], playerData[8]);
    Loader('hide');
}

function WebtvVideoPlay(flag)
{
    //DeviceConsole('WebtvVideoPlay');
    // url, poster, id, video_pos_id, last_resume_position, timezone, content_type, ttml_file_url, ttml_language 
    var fcon = document.getElementById('fcon');
    var playerData = fcon.getAttribute('videoData').split('^^');

    if (flag != 1) playerData[4] = 0;
    WebtvShowVideoPlayerAndPlay(playerData);
}

function ShowResumeOptionX(resumeOffset)
{
    if (resumeOffset==undefined || resumeOffset==0) return 0;
    var resumeOptionObj = GetInputElementByEType('resumeplay');   
    if (resumeOptionObj==null) return 0;
    $(resumeOptionObj).show();

    SetLastIndex(-1);
    ki = resumeOptionObj.getAttribute('keyindex');
    SetActiveElementByIndex(ki);
    return 1;
}

function WebtvVideoReady2(status, responseText)
{
    DeviceConsole('WebtvVideoReady2: ' + status + ', responseText: ' + responseText);

    // url, poster, id, video_pos_id, last_resume_position, timezone, content_type, ttml_file_url, ttml_language 
    var msg = document.getElementById('fcon').getAttribute('videoData');
    var playerData = msg.split('^^');

    if (status == 200) 
    {
        var ans = ShowResumeOptionX(playerData[4]);
        if (ans==1) Loader('hide'); 
        else WebtvShowVideoPlayerAndPlay(playerData); 
    }
    else 
    {
        Loader('hide');
        //ShowAlert('Connection Error', 'stream offline, please try again later', 300);
        if (playerData[0].indexOf('https:') != -1) playerData[0] = 'https://down.vodroute.com/offline/offline.mp4/playlist.m3u8';
        else playerData[0] = 'http://down.vodroute.com/offline/offline.mp4/playlist.m3u8';
        WebtvShowVideoPlayerAndPlay(playerData); 

    }
    return;
}

function StreamAccessible(stream, callback)
{
    if (stream.substr(0,2)=='ws') {
        WebtvVideoReady2(200, '');
        return;
    }

    //if (typeof(callback)=='function') { callback(200, ""); return; }

    //DeviceConsole('StreamAccessible: ' + stream);
    const Http = new XMLHttpRequest();
    Http.onreadystatechange = function() 
    {
        //DeviceConsole('onreadystatechange: ' + this.readyState + ' (' + this.status + ')');
        if (this.readyState==4)
        {
            if (typeof(callback)=='function') callback(this.status, this.responseText);
        }
    }
    Http.open("GET", stream, true);
    Http.send();
}

function WebtvVideoReady(msg)
{
    LogTime('WebtvVideoReady');
    //DeviceConsole(msg);
    // url, poster, id, video_pos_id, last_resume_position, timezone, content_type, ttml_file_url, ttml_language 
    var fcon = document.getElementById('fcon');
    var vdata = $(fcon).attr('videoData');
    var playerData = msg.split('^^');
    //DeviceConsole(playerData);
    if (playerData[0]=='') return;
 
    if (typeof(vdata)=='string')
    {
        var videoData = vdata.split('^^');
        while(videoData.length < 9) videoData.push('');
        //DeviceConsole('WebtvVideoReady :' + playerData[2] + '['+playerData[5]+']  == ' + videoData[2] + '['+videoData[5]+']');
        if (videoData[2] == playerData[2] && videoData[5] == playerData[5])
        {
            if (appVersion >= 118) {
                var vp1 = GetInputElementByEType('video', 0);
                if (vp1.getAttribute('mode')=='1') Device_SetScreenSize(1);
                return;
            }
            var vp1 = document.getElementById('videopanel1').parentElement;
            var mode = vp1.getAttribute('mode');
            if (mode=='1') Device_SetScreenSize(1);
            return;
        }
    }

    fcon.setAttribute('videoData', msg);
    if(playerData[6]=='trailer') WebtvVideoReady2(200, "");
    else StreamAccessible(playerData[0], WebtvVideoReady2);
    return;
}

function WebtvVideo(param0)
{
    //DeviceConsole('WebtvVideo');
    // check if videopanel1 exists or not
    if (appVersion < 120) {
        var vp1 = document.getElementById('videopanel1');
        if (vp1==null) {
            var htmlString = '';
            htmlString = "<div style='position:absolute;left:0px;top:0px;width:100%;height:100%;' etype='video' mode='0'>";
            htmlString += "<div id='videopanel1' style='position:absolute;left:0px;top:0px;width:100%;height:100%;display:none;'></div>";
            htmlString += "</div>";
            var div = document.createElement('div');
            div.innerHTML = htmlString;
            document.getElementById('fcon').appendChild(div.firstChild);
        }
    }

    if (DEVICEID==12) Loader('show');  // formag devicesonly as they have sluggish response

    LogTime('WebtvVideo');
    AjaxCall('&action=getStreamUrl'+param0 + GetPotionK(), WebtvVideoReady, '', DeviceConsole); 
}

function WebtvVideoX(ctype, panelObj, timezone)
{
    WebtvVideo('&ctype='+ctype+'&id='+$(panelObj).attr('cid')+'&tz='+timezone); 
}

//==============================================================================
function PassKeyEvent(keycode)
{
    // this function is called by android app to pass key events to webapp

    //DeviceConsole('PassKeyEvent :' + keycode + ', screenIndex :' + screenIndex);

    if (appVersion < 118 || DEVICEID==4) 
    {
        if (IsVideoPlaying()==1) { 
            var vp1 = document.getElementById("videopanel1");

            //DeviceConsole('IsVideoPlaying');
            if (keycode=='playpause' && $(vp1).attr('playpause')==0) {
                var fcon = document.getElementById('fcon');
                if (fcon.getAttribute('state')=='play') { fcon.setAttribute('state','pause'); Device_PauseVideo(); }
                else { fcon.setAttribute('state', 'play'); Device_ResumeVideo(); }
                return 13; 
            }
            else if (keycode=='stop') { CloseVideo("back"); return 13; }
            else if (keycode=='fastforword' && $(vp1).attr('ffrw')==0) { Device_FastForword(1); return 13; }
            else if (keycode=='rewind' && $(vp1).attr('ffrw')==0) { Device_FastBackword(1); return 13; }
            else if (keycode=='fullscreen') { Device_FullScreen(); return 13; }
            else if (keycode=='back' || keycode=='home') { CloseVideo("back"); return 13; }
        }
    }
    else {
        if (IsVideoPlaying()==1) 
        { 
            if (keycode=='home' && typeof(Device_HomeKeyAction)=='function') { 
                Device_HomeKeyAction(); 
                return 13; 
            }

            var vp1 = GetInputElementByEType('video', 0);
            //DeviceConsole(vp1.getAttribute('controls')); 
            if (vp1.getAttribute('controls')=='0') 
            {
                if (keycode=='ff') { Device_FastForword(1);  return 13; }
                else if (keycode=='rewind') { Device_FastBackword(1);  return 13; }
                else if (keycode=='playpause') { Device_PauseVideo();  return 13; }

                if (Device_PlayerController('isvisible')==1) 
                {
                    if (keycode=='right') { 
                        var c = Int(vp1.getAttribute('c'));
                        if (c < 4) {
                            c++; 
                            Device_PlayerControllerButton(c); 
                            vp1.setAttribute('c', c);
                        }
                        return 13; 
                    }
                    else if (keycode=='left') { 
                        var c = Int(vp1.getAttribute('c'));
                        if (c > 0) {
                            c--; 
                            Device_PlayerControllerButton(c); 
                            vp1.setAttribute('c', c);
                        }
                        return 13; 
                    }
                    else if (keycode=='up') { Device_PlayerController('hide');  return 13; }
                    else if (keycode=='OK') { 
                        var c = Int(vp1.getAttribute('c'));
                        if (c==0) MoveToChannel(-1);  
                        else if (c==1) Device_FastBackword(1); 
                        else if (c==2) Device_PauseVideo(); 
                        else if (c==3) Device_FastForword(1);  
                        else if (c==4) MoveToChannel(1);  
                        return 13; 
                    }
                }
                else if (keycode=='down') { Device_PlayerController('show'); return 13; }
            }
        }
    }
    return OnKeyDownEvent2(keycode);
}

function MoveToChannel(keycode, previewMode)
{
    //DeviceConsole('MoveToChannel: '+keycode);
    if (typeof(screenIndex) === 'undefined') return 0;

    var panelObj = document.getElementById('screen_container'+screenIndex);
    if (panelObj==null) return 0;

    var pObj = $(panelObj).closest('#browse_container');
    if (pObj == null) return 0;

    var px = pObj.attr('parent');
    var parent = document.getElementById('ppanel'+px);
    var etype = parent.getAttribute('etype');
    var ro = Int(parent.getAttribute('ro'));
    var ci = Int(parent.getAttribute('ci'));
    //DeviceConsole('MoveToChannel: ' + etype + ', ro: ' + ro + ', ci: ' + ci);

    if (etype=='rowlist') 
    { 
        ci = ci + keycode;
        var cimax = $('#R'+px+'_'+ro).attr('last');
        if (ci < 0 || ci >= cimax) return 0;

        if (keycode==-1) OnKeyDownEvent__(parent, 'left', 1);
        else if (keycode==1) OnKeyDownEvent__(parent, 'right', 1);

        if (previewMode == undefined || previewMode==0) {
            GetActionUrl(px + '_' + $(parent).attr('ro') + '_' + $(parent).attr('ci'), 'keep'); 
        }
    }
    return 0;
}

function OnWebTVKeyEvent(e)
{
    DeviceConsole('OnWebTVKeyEvent');
    var keycode = Device_GetKeyCode(e);
    if (keycode=='') return; 

    e.preventDefault();
    PassKeyEvent(keycode);
}

function OnKeyUp(e)
{
    if (appVersion < 118) return 0;

    var keycode = Device_GetKeyCode(e);
    if (keycode=='') return; 

    e.preventDefault();
    //DeviceConsole('OnKeyUp :' + keycode + ', screenIndex :' + screenIndex);

    if (IsVideoPlaying()==1) { 
        var vp1 = GetInputElementByEType('video', 0);
        if (keycode=='ff' && $(vp1).attr('ffrw')==0) { Device_FastForword(0); return 13; }
        else if (keycode=='rewind' && $(vp1).attr('ffrw')==0) { Device_FastBackword(0); return 13; }
    }
}

function AddToFavorite(cid, isbinge, bingeid) { return AjaxCallWaitForResponse('&action=add2Favorite&isbinge=' + isbinge + '&bingeid=' + bingeid + '&videoid=' + cid + GetPotionK(), '', DeviceConsole);           }
function RemoveFromFavorite(favid)   { return AjaxCallWaitForResponse('&action=removeFromFavorite&favid=' + favid + GetPotionK(), '', DeviceConsole);     }

function GoBack(where)
{
    DeviceConsole('GoBack: '+where);
    if (IsVideoPlaying()==1) {
        if (appVersion < 118) CloseVideo('back');
        else {
            if (where == undefined) where = 'none';
            CloseVideo(where);
        }
    }
    else 
    {
        var fcon = document.getElementById('fcon');
        if (where == 'parent')
        {
            DeviceConsole('scObj = screen_container'+screenIndex);
            var scObj = document.getElementById('screen_container'+screenIndex);
            if (scObj!=null) {
                var bcon = scObj.parentElement;

                DeviceConsole('ppanel = ppanel'+$(bcon).attr('parent'));
                var ppanel = document.getElementById('ppanel'+$(bcon).attr('parent'));
                var scObj2 = $(ppanel).closest('[activeindex]');
                var si = scObj2.attr('id').substring(16);
                DeviceConsole(si);

                RemoveJsCssCode('js', bcon.id);
                fcon.removeChild(bcon);
                SetScreenIndex(si);
            }
            return;
        }

        if (fcon.childNodes.length > 1)
        {
            var lastScreen = fcon.childNodes[fcon.childNodes.length-1];
            if ($(lastScreen).attr('etype')=='video') {
                fcon.removeChild(lastScreen);
                lastScreen = fcon.childNodes[fcon.childNodes.length-1];
            }

            var clevel = lastScreen.getAttribute('level');
            if (clevel=='form') {
                RemoveJsCssCode('js', lastScreen.id);
                //DeviceConsole('GoBack (removeChild): '+lastScreen.id + ', clevel: '+clevel);  
                fcon.removeChild(lastScreen);
            }
            else {
                while(fcon.childNodes.length > 0) {
                    lastScreen = fcon.childNodes[fcon.childNodes.length-1];
                    if (lastScreen.getAttribute('level') == clevel) {
                        RemoveJsCssCode('js', lastScreen.id);
                        DeviceConsole('GoBack (removeChild): '+lastScreen.id + ', clevel: '+clevel);  
                        fcon.removeChild(lastScreen);
                    }
                    else break;
                }
            }
        }
        var lastScreen = GetLastScreen();
        if (lastScreen != null) 
        {
            // check if any screen is visible already
            var clevel = lastScreen.getAttribute('level');
            DeviceConsole(clevel);
            for (var i = fcon.childNodes.length-1 ; i >= 0 ; i--) {
                var cScreen = fcon.childNodes[i];
                if (cScreen.getAttribute('level') == clevel && cScreen.style.display=='block') { 
                    SetActiveScreenIndex(cScreen.id); 
                    SetFocus();
                    return 13;
                }
            }
            SetActiveScreenIndex(lastScreen.id);
            lastScreen.style.display='block';
            SetFocus();
        }
    }
    return 13;
}

function SetVideoPos(vpos)
{
    var videoposid = document.getElementById('fcon').getAttribute('videoposid');
}

function loginHandler(msg)
{
    Loader('hide');
    $('#login').attr('status', '0');

    //DeviceConsole('loginHandler: ' + msg);
    var parts = msg.split('^');
    potionK = parts[2];
    if (parts[0]=='success') {
        PopLastScreen(0, 0);
        InitTV();
    }
    else if (parts[0]=='error') ShowAlert('', parts[1], 300);
    else if (parts[0]=='errorscreen') 
    {
        var eparts = parts[1].split('|');
        var templateName = eparts[0].replace('-nomenu', '');
        //var url = '&formType=login&layoutName=' + templateName;
        var url = '&formType='+templateName+'&layoutName=' + templateName;
        WebTVForm(url, 'active', 0);
    }
}

function resetpasswordHandler(msg)
{
    Loader('hide');
    $('#login').attr('status', '0');

    var parts = msg.trim().split('^');
    if (parts[0]=='success') ShowAlert(parts[1], parts[2], 300);
    else if (parts[0]=='error' && parts[1] != '') ShowAlert(parts[1], parts[2], 300);
}

function resetpassword(ndx)
{
    var status = $('#login').attr('status');
    if (typeof(status)=='string' && status=='1') return;

    Loader('show');
    $('#login').attr('status', '1');

    AjaxCall('&action=resetPassword&ndx='+ Str2Hex(ndx) + GetPotionK(), resetpasswordHandler, "");
}

function findusername(ndx)
{
    var status = $('#login').attr('status');
    if (typeof(status)=='string' && status=='1') return;

    Loader('show');
    $('#login').attr('status', '1');

    AjaxCall('&action=findUser&ndx='+ Str2Hex(ndx) + GetPotionK(), resetpasswordHandler, "", DeviceConsole);
}

function login(ndx)
{
    var status = $('#login').attr('status');
    if (typeof(status)=='string' && status=='1') return;

    Loader('show');
    $('#login').attr('status', '1');

    //DeviceConsole('login: '+ndx);
    var parts = ndx.split(',');
    Device_SetKeyValue(parts[0], parts[1]);
    Device_SetKeyValue(parts[2], parts[3]);
    AjaxCall('&action=connect&ndx='+ Str2Hex(ndx) + GetPotionK(), loginHandler, "", DeviceConsole);
}

function logout_(ndx) 
{
    Loader('show');
    AjaxCall('&action=disconnect' + GetPotionK(), function(msg) { window.top.location.reload(true); }, "");
}

function logout(ndx) 
{
    if (appVersion >= 120)
    {
        var btnObj = [ { text: "Unlink", click: function() { $('#myDialogContainer').hide(); logout_(''); } },
                       { text: "Cancel", click: function() { $('#myDialogContainer').hide(); FocusToControl('set'); } }
                     ] 

        ShowAlert('Unlink Device', 'This device will be unlinked from your account', 300, btnObj);
        return;
    }
    logout_(ndx);
}

function reload(ndx)
{
    PopLastScreen(0, 0);
    //window.top.location.reload(true);
    window.location.reload(true);
}

function ShowScreen(layoutname, ndx, x)
{
    var parent = $('#ppanel'+x).closest('[level]');
    var lvl = '';
    if (parent != undefined) lvl = Int(parent.attr('level'))+1; 

    //DeviceConsole('ShowScreen: '+layoutname+', ndx: '+ndx);
    var p = '&formType='+layoutname+'&layoutName='+layoutname;
    WebTVForm(p, 'active', lvl);
}

function cancel(ndx)
{
    PopLastScreen(1,1);
}

function go_back(ndx)
{
    DeviceConsole('go_back');
    PassKeyEvent('back');
}

function GetDataFromLocalStorage(key)
{
    var v = Device_GetKeyValue(key);
    if (typeof(v) != 'undefined') document.getElementById(key).value = v;
}

//==============================================================================
function OnKeepMeAlive(msg) 
{  
/*
    if (Int(msg)==-1) 
    {
        ShowAlert('', 'You have been logged out...', 300);
        window.top.location.reload(true);
        return 0;
    }
*/
    window.setTimeout('KeepMeAlive()', 1000*120);  
}
function KeepMeAlive() {  AjaxCall('&action=keepmealive' + GetPotionK(), OnKeepMeAlive, '');  }

//==============================================================================
function SetActiveScreenIndex(containerid)
{
    DeviceConsole('SetActiveScreenIndex: '+containerid);
    var vp1 = document.getElementById(containerid);
    if (vp1 == null) return 0;
   
    for (var k=0; k < vp1.children.length; k++) {
        if (vp1.children[k].hasAttribute('activeIndex')) {
            SetScreenIndex( vp1.children[k].id.substring(16) );
            //screenIndex = vp1.children[k].id.substring(16);
            Device_Init({action:'setview', data: containerid });
            return 1;
        }
    }
    return 0;
}

function CleanUpScreens(minlevel, containerid, status)
{
    //DeviceConsole(['CleanUpScreens',minlevel, containerid]);
    var fcon = document.getElementById('fcon');
    var clen = fcon.childNodes.length;

    // turn off all screens of same and above current level
    for (var i = 0 ; i < clen ; i++) {
        var lvl = Int(fcon.childNodes[i].getAttribute('level'));
        if (lvl < minlevel) continue;
        else if (lvl > minlevel) fcon.childNodes[i].style.display='none';
        else {
            if (fcon.childNodes[i].id == containerid) $(fcon.childNodes[i]).show();
            else fcon.childNodes[i].style.display='none';
        }
    }

    // turn off video if it is playing  
    if (IsVideoPlaying()==1 && status != 'keep') Device_StopVideo(''); 

    // delete existing 'browse_container' screen if new screen is NOT 'browse_container' screen
    if (containerid != 'browse_container' && fcon.childNodes.length > 0)
    { 
        var videoScreen = fcon.childNodes[fcon.childNodes.length-1];
        if (videoScreen.id=='browse_container') {
            RemoveJsCssCode('js', 'browse_container');
            fcon.removeChild(videoScreen);
        }
    }
}

function CreateDivView(divid, level, parent)
{
    //DeviceConsole(['CreateDivView', divid, level, parent]);
    if (level==undefined) level='0';
    var htmlString = "<div id='"+divid+"' level='"+level+"' parent='"+parent+"'></div>";
    var div = document.createElement('div');
    div.innerHTML = htmlString;
    return div.firstChild; 
}

function ShowWebTVSecreen(htmlJs, status)
{
    var fcon = document.getElementById('fcon');
    var clen = fcon.childNodes.length;

    if (htmlJs.level != 'form') CleanUpScreens(htmlJs.level, htmlJs.container_id, status);

    // check if screen exists before appending it 
    var vp1 = document.getElementById(htmlJs.container_id);
    if (vp1==null) {
        var div = CreateDivView(htmlJs.container_id, htmlJs.level, htmlJs.parent);
        document.getElementById('fcon').appendChild(div);
    }
    else vp1.style.display='block';
}

function JsLoaded()
{
    DeviceConsole('************** js loaded ***********************');
}

function RenderWebTVScreen_(htmlJs)
{
    potionK = htmlJs.potionK;
    //DeviceConsole(htmlJs);
    //DeviceConsole('RenderWebTVScreen_: ' + htmlJs.status + ', autoplay: ' + htmlJs.autoplay);
    var fcon = document.getElementById('fcon');
    ShowWebTVSecreen(htmlJs, htmlJs.status);
    Loader('hide');

    // save current screenindex 
    var currentSI = screenIndex;

    var cssLinks = htmlJs.cssLinks.split(';');
    for (var k = 0 ; k < cssLinks.length ; k++) LoadJsCssFile(cssLinks[k], 'css');

    var jsLinks = htmlJs.jsLinks.split(';');
    for (var k = 0 ; k < jsLinks.length ; k++) LoadJsCssFile(jsLinks[k], 'js');

    RemoveJsCssCode('js', htmlJs.container_id);
    var containerObj = document.getElementById(htmlJs.container_id);
    if (containerObj != null) containerObj.innerHTML = htmlJs.html;
    LoadJsCssCode(htmlJs.js, 'js', htmlJs.container_id, JsLoaded);


    if (htmlJs.autoplay != '')
    {
        var idTz = htmlJs.autoplay.split('^');
        while(idTz.length < 4) idTz.push('');
        var vparam = '&ctype='+idTz[0]+'&id='+idTz[1]+'&tz='+idTz[2]+'^'+idTz[3];
        window.setTimeout("WebtvVideo('"+vparam+"')", 100);
        //window.setTimeout("WebtvVideo('"+htmlJs.autoplay+"')", 100);
        WebTVBrowseId = 0;
        return;
    }

    WebTVBrowseId = 0;
    //DeviceConsole('RenderWebTVScreen => status: ' + htmlJs.status + ', container_id: ' + htmlJs.container_id + ', layout: ' + htmlJs.layout + ', screenIndex: ' + screenIndex + ', currentSI: ' + currentSI);
    if (htmlJs.layout=='yes') {
        if (htmlJs.status=='active' || htmlJs.status=='keep') {
            if (currentSI == screenIndex) return;
            //if (htmlJs.elemlist != '') Device_Init({action: 'inputcontrols', data: htmlJs.elemlist });
            Device_Init({action:'setview', data: htmlJs.container_id });
            FocusToControl('set');
        }
        //else screenIndex = currentSI;
    }
    else window.setTimeout('GoBack()', 1000); 
    //DeviceConsole('***************************** '+screenIndex);
    WebTVBrowseId = 0;
}

function RenderWebTVScreen(msg)
{
    //DeviceConsole('RenderWebTVScreen');
    //DeviceConsole(msg);
    htmlJs = JSON.parse(msg);
    RenderWebTVScreen_(htmlJs);
}

//==============================================================================
function WebTVDetails_(paramString)
{
    //LogTime('WebTVDetails_: '+paramString);

    var par1 = 'action=getContentDetails';
    if (paramString.indexOf('&active=')==-1) par1 += '&active=active';
    par1 += '&id='+ paramString + GetPotionK();
    //DeviceConsole('WebTVDetails_: ' + paramString);
    AjaxCall(par1, RenderWebTVScreen, '', DeviceConsole); 

    var fcon = document.getElementById('fcon');
    fcon.setAttribute('videoData', '^^^^^^^^^^^^^^^^');
}

function WebTVDetails(paramString, action)
{
    //DeviceConsole('WebTVDetails: ' + paramString + ', action: ' + action);
    if (action=='keep') {
        WebTVDetails_(paramString+'&active=keep');
        return 0;
    }

    if (IsVideoPlaying()) {
        PassKeyEvent('stop');
        window.setTimeout("WebTVDetails_('"+paramString+"')", 100);
    }
    else WebTVDetails_(paramString);
}


function WebTVDetailsReplace2(k)
{
    Device_VideoStopVideo('main_player');
    WebTVDetails_(k);
}

function WebTVDetailsReplace(panelObj, cid)
{
    var vFlag = $(panelObj).attr('video');
    var px = $(panelObj).closest('[level]').attr('parent');
    var scontainer = $(panelObj).closest('[activeindex]')[0];
    var cx = scontainer.children[0].getAttribute('x')+'^'+scontainer.id.substring(16)+'^'+$(scontainer).attr('dccrid');
    var k = cid+'&parent='+px;    
    Device_VideoStopVideo('channelgallery');
    WebTVDetailsReplace2(k);
}


function WebTVForm(paramString, makeactive, level)
{
    if (makeactive==undefined) makeactive='active';
    //DeviceConsole('WebTVForm: '+paramString);
    AjaxCall('action=showForm&active=' + makeactive + '&level=' + level + GetPotionK() + '&'+ paramString, RenderWebTVScreen, '', DeviceConsole); 
}

function ScreenExists(containerid)
{
    //DeviceConsole('ScreenExists: ' + containerid);
    var vp1 = document.getElementById(containerid);
    if (vp1 == null) return 0;
    if (vp1.getAttribute('parent')=='system') return 0;
    return 1;
}

function WebTVBrowse(cid, level, makeactive)
{
    if (makeactive==undefined) makeactive='active';
    DeviceConsole('WebTVBrowse : ' + cid + ', ' + level + ', ' + makeactive + ', WebTVBrowseId: ' + WebTVBrowseId);

    if (WebTVBrowseId != 0) return;

    var cid2 = cid;
    if (Int(cid2) > 0)
    {
        while(cid2.length < 7) cid2 = '0'+cid2;
    } 

    var fcon = document.getElementById('fcon');
    fcon.setAttribute('videoData', '^^^^^^^^^^^^^^^^');

    // check if screen exists before appending it                
    if (ScreenExists('browse_'+cid2)==1) {
        // save current screenindex 
        var currentSI = screenIndex;
        if (makeactive=='active') 
        {
            if (SetActiveScreenIndex('browse_'+cid2)==1)
            {
                CleanUpScreens(level, 'browse_'+cid2, '');
                $('#browse_'+cid2).show();
                if (appVersion >= 118) {
                    var fnname1 = 'OnReloadScreen_'+screenIndex;
                    if (typeof(window[fnname1])=='function') {
                        if (window[fnname1](screenIndex, cid)==1) return;       
                    }
                }
                FocusToControl('set');
            }
        }
        else if (makeactive=='visible') 
        {
            CleanUpScreens(level, 'browse_'+cid2, '');
            //$('#browse_'+cid2).show();
        }
        return;
    }

    Loader('show');
    WebTVBrowseId = cid;
    AjaxCall('action=getChildList' + GetPotionK() + '&id='+cid+'&level='+level + '&active=' + makeactive, RenderWebTVScreen, '', DeviceConsole); 
}

function VirtualRemoteClicked(e)
{
    e.stopPropagation();
    if (e.target.id != '')
    {
        if (e.target.id=='vrright') PassKeyEvent('right');
        else if (e.target.id=='vrleft') PassKeyEvent('left');
        else if (e.target.id=='vrup') PassKeyEvent('up');
        else if (e.target.id=='vrdown') PassKeyEvent('down');
        else if (e.target.id=='vrok') PassKeyEvent('OK');
        else if (e.target.id=='vrback') PassKeyEvent('back');
        else if (e.target.id=='vrclose') $('.virtualRemote').hide();
    }
}

function ShowAlert(header, message, m_width, buttonArray)
{
    if (appVersion >= 120) 
    {
        if ($('#myDialogContainer').is(':visible') == true) return;

        $('#webtvDBTitleBox').html(header);
        $('#webtvDBMessageBox').html(message);

        var btnObj = { text: "Ok", click: function() { $('#myDialogContainer').hide(); FocusToControl('set'); } };
        if (buttonArray==undefined) buttonArray = [ btnObj ];

        var buttons = '';
        for (var i = 0 ; i < buttonArray.length ; i++) {
            if (i==0) buttons += "<div class='webtvDBButton webtvDBButtonA'>" + buttonArray[i].text + "</div>";
            else buttons += "<div class='webtvDBButton'>" + buttonArray[i].text + "</div>";
        }
        var bbox = document.getElementById('webtvDBButtonBox');  
        bbox.innerHTML = buttons;
        
        for (var i = 0 ; i < bbox.children.length ; i++) {
            bbox.children[i].onclick = buttonArray[i].click;
        }

        $('#myDialogContainer').attr('c', '0');
        $('#myDialogContainer').show();
        return; 
    }

    if (document.getElementById('fcon').getAttribute('alert')=='1') return;

    if (m_width==undefined) m_width=300;
    if (buttonArray==undefined) buttonArray=[];
    FocusToControl('remove');

    $('#dialogContainer').attr('title', header);
    $('#dialogContainer').html("<p>"+message+"</p>");

    var btnObj = { text: "Ok", 
                   click: function() { 
                                        $(this).dialog("close"); 
                                        FocusToControl('set');
                                     } 
                 };
    var myButtons = [];
    if (buttonArray.length > 0) 
    {
        for (var i = 0 ; i < buttonArray.length ; i++) myButtons.push(buttonArray[i]);
    }
    else myButtons.push(btnObj);

    $('#dialogContainer').attr('bc', myButtons.length);
    $('#dialogContainer').attr('c', 0);

    $( "#dialogContainer" ).dialog({ 
        modal:true,
        width: m_width, 
        buttons: myButtons,            
        close:function( event, ui ) { 
            $('#dialogContainer').html('');  
            document.getElementById('fcon').setAttribute('alert', '0');
            FocusToControl('set');
        }  
    });

    document.getElementById('fcon').setAttribute('alert', '1');
    return;
}

function ProgramListSetReminder()
{
    var pListObj = GetInputElementByEType('programlist', 0);

    var obj = document.getElementById( $(pListObj).attr('x') + '_' + $(pListObj).attr('ro') );
    if (obj != null) 
    {
        var sMin = Int(obj.getAttribute('s')) / 60;
        var ymd = pListObj.getAttribute('ymd');
        AjaxCallWaitForResponse('&action=setReminder&d=' + ymd + '^' + sMin + '^' + obj.getAttribute('cid') + '^' + '7');
    }
}

function PopulateChannelGallery(msg)
{
    var parts = msg.split('@@');
    var panelObj = document.getElementById('ppanel'+parts[0]);
    document.getElementById('owl'+parts[0]).innerHTML = parts[1];
    panelObj.setAttribute('ro', parts[2]);
}

function GetChannelGalleryData(x)
{
    var panelObj = document.getElementById('ppanel'+x);
    AjaxCall('action=getChannelGalleryData&x='+x+'&dccrid='+$(panelObj).attr('cid')+'&type='+$(panelObj).attr('type') + GetPotionK(), PopulateChannelGallery, '', DeviceConsole);
}

function ResizeScreen(si)
{
    var fname = 'onresize_' + si; 
    DeviceConsole(fname+': '+typeof(window[fname]));
    if (typeof(window[fname])=='function') window[fname]();

    // first div child contains attribute ss which indicates if this screen is fullscreen or has screensize 
    var ss = $('#browse_'+si+' > div:first').attr('ss');
    if (ss==0) return '';

    // get parentId of this screen
    var parentId = $('#browse_'+si).attr('parent');
    if (parentId==0) return '';

    // search entire DOM tree under fcon for element whose attribute cid has value parentId
    var sel = "[cid='" + parentId + "']";
    var pObj = $('#fcon').find(sel).closest('[parent]');

    if (pObj==null) return '';
    var pidStr = pObj.attr('id');
    //DeviceConsole(pidStr);
    if (pidStr==null) return '';
    return pidStr.substr(7);
}

window.onresize = function()
{
    if (GetFromPotionK('device_id') != 4) return; 

    var si = screenIndex;
    while(si != '')
    {
        si = ResizeScreen(si);
    }

    var level = Int($('#browse_'+screenIndex).attr('level'))+1;
    var sel = "[level='" + level + "']";
    $('#fcon').find(sel).each(function() {
        var si0 = $(this).attr('id').substr(7);
        //console.log(si0);
        if ($(this).is(':visible')==true) {
            ResizeScreen(si0);
        }
    })
}
