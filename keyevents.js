var myClick = [0,0,0,''];
var clickEventProcessed=0;
var scrollingEvent = { scrollTimer:null, me:'', updating:0 };
var LASTITEM = 9999999;

function SetLastIndex(aindex)
{
    if (aindex==-1) aindex = $('#screen_container'+screenIndex).attr('activeIndex');
    $('#screen_container'+screenIndex).attr('lastIndex', aindex);
}

function GetLastIndex()
{
    return $('#screen_container'+screenIndex).attr('lastIndex');
}


function SetActiveElementByIndex(aindex)
{
    //DeviceConsole('SetActiveElementByIndex: '+aindex);
    $('#screen_container'+screenIndex).attr('activeIndex', aindex);
    FocusToControl('set');
}

function SetActiveElement(obj)
{
    //DeviceConsole('SetActiveElement');
    if (obj==null) return;

    var objIndex = obj.getAttribute('keyindex');
    var aIndex = $('#screen_container'+screenIndex).attr('activeIndex');
    //DeviceConsole('SetActiveElement; '+objIndex+', '+aIndex);
    if (aIndex != objIndex) 
    {
        var panelObj = GetInputElementByIndex(-1);
        if (panelObj != null) FocusToControl__('remove', panelObj);

        SetActiveElementByIndex(objIndex);
    }
}

function GetElementsBy_(what, v)
{
    var elist = [];
    if (typeof(screenIndex) === 'undefined') return elist;
    var scObj = document.getElementById('screen_container'+screenIndex);
    if (scObj==null) return elist;

    var elen = scObj.children.length;  
    var c=-1;
    for(var i=0 ; i<elen ; i++)
    {
        obj = scObj.children[i];
        if (obj == null) continue;
        if ($(obj).attr(what) == v) elist.push(obj);
    }
    return elist;
}

//==============================================================================

function GetScreenElements(scObj)
{
    var elements = [];
    var e_len = scObj.children.length;  
    for(var i=0 ; i<e_len ; i++)
    {
        obj = scObj.children[i];
        if (obj == null) continue;

        elements.push(obj);
        var etype = obj.getAttribute('etype');
        if (typeof(etype)=='string' && etype=='groupbegin') 
        {
            var cObj = GetScreenElements(obj);
            if (cObj != null) elements = elements.concat(cObj);
            continue;
        } 
    }
    return elements;
}

function GetInputElementByIndex(aIndex)
{
    //if (aIndex==-1) DeviceConsole('GetInputElementByIndex: ' + aIndex + ' , screenIndex: '+screenIndex);
    if (typeof(screenIndex) === 'undefined') return null;

    var scObj = document.getElementById('screen_container'+screenIndex);
    if (scObj==null) return null;

    if (aIndex==-1) aIndex = scObj.getAttribute('activeIndex');

    //DeviceConsole('GetInputElementByIndex: '+aIndex);
    var elements = GetScreenElements(scObj);
    var e_len = elements.length;  
    for(var i=0 ; i<e_len ; i++)
    {
        obj = elements[i];
        if (obj.hasAttribute('keyIndex')==false) continue;
        if (obj.getAttribute('keyIndex') == aIndex) return obj;
    }
    return null;
}

//==============================================================================

function GetInputElementByEType(etype, aIndex, si)
{
    //DeviceConsole('GetInputElementByEType('+etype+', '+aIndex+', '+si);

    if (typeof(si) == 'undefined') si=screenIndex;
    if (typeof(si) == 'undefined') return null;
    if (typeof(aIndex) == 'undefined') aIndex=0;

    var scObj = document.getElementById('screen_container'+si);
    if (scObj==null) return null;
    if (etype=='screen') return scObj;

    var elements = GetScreenElements(scObj);
    var e_len = elements.length;  
    var c=-1;
    for(var i=0 ; i<e_len ; i++)
    {
        var obj = elements[i];
        if (obj.hasAttribute('etype')==false) continue;
        if (obj.getAttribute('etype') != etype) continue;
        c++;

        if (c==aIndex) return obj;
    }
    return null;
}

function HideGroup(groupid)
{
    var obj = document.getElementById(groupid);  
    if (obj != null) obj.setAttribute('t0', obj.getAttribute('t1'));
}

function ShowGroup(groupid)
{
    //DeviceConsole('ShowGroup: '+groupid);
    var obj = document.getElementById(groupid);  
    if (obj != null) {
        var fname = 'Gb1' + obj.getAttribute('x');
        //DeviceConsole(fname + ' = ' + typeof(window[fname]));
        if (typeof(window[fname])=='function') window[fname]();
        $(obj).show();
    }
}

function AObject(etype, aIndex, si) {  return GetInputElementByEType(etype, aIndex, si);  }

function AGetCategoryContents(pid)
{
    return AjaxCallWaitForResponse('action=getCategoryContents' + GetPotionK() + '&id='+pid, '', DeviceConsole); 
}

function AShow(etype, aIndex, si)
{
    var obj = GetInputElementByEType(etype, aIndex, si)
    if (obj != null) $(obj).show();
}

function AHide(etype, aIndex, si)
{
    var obj = GetInputElementByEType(etype, aIndex, si)
    if (obj != null) $(obj).hide();
}

function AToggle(etype, aIndex, si)
{
    //DeviceConsole('AToggle: '+etype+'-'+aIndex+'-'+si);
    var obj = GetInputElementByEType(etype, aIndex, si);
    if (obj != null) {
        //DeviceConsole('AToggle visibility: '+obj.style.display);
        if (obj.style.display=='none') { $(obj).show();  return 'visible'; }
        else { $(obj).hide(); return 'hidden'; }
    }
    return '';
}

function AStopVideo(playerid)
{
    //DeviceConsole('AStopVideo');
    return Device_VideoStopVideo(playerid);
}

function ALevel(si)
{
    return $('#screen_container'+si).parent().attr('level');
}

function ATranslation_(obj, properties, duration)
{
    //DeviceConsole('ATranslation_');
    if (obj==undefined || obj==null) return 1;
    $(obj).animate( properties, duration);
    return 1;
}

function ATranslation(obj, property, start, end, duration, unit)
{
    if (obj==undefined || obj==null) return 1;
    var cp = Double(obj.style[property]);
    var change = (end - start) / (duration * 200);
    if (change < 0 && cp <= end) return 1;
    if (change > 0 && cp >= end) return 1;

    var newValue = (cp + change) + unit;
    obj.style[property] = newValue;
    return 0;
}

function ExternalPoster(obj)
{
    //DeviceConsole('ExternalPoster');
    var x = obj.getAttribute('x');
    var itemid = x + '_' + obj.getAttribute('ro') + '_' + obj.getAttribute('ci');

    var pobj = document.getElementById('ppanel'+ x +'_poster');
    if (pobj != null) {
        if (obj.getAttribute('etype')=='postergallery') {
            pobj.children[0].src = document.getElementById(itemid).src;
        }
        else {
            var src = document.getElementById(itemid).getAttribute('data');
            pobj.src=src;
        }
    }

    pobj = document.getElementById('ppanel'+ x +'_label');
    if (pobj != null) {
        var src = ' > .ppanel'+x+'_text';
        pobj.innerHTML = $('#'+itemid+src).html();
    }
}

function SetScreenIndex(m)
{
    //DeviceConsole('SetScreenIndex: ' + m + ', existing: ' + screenIndex);
    if (m==screenIndex) return;

    var fnname1 = 'Animation_'+screenIndex;
    var fnname2 = 'Animation_'+m;
    //DeviceConsole('SetScreenIndex remove: '+fnname1+'('+typeof(window[fnname1])+'), set: '+fnname2+'('+typeof(window[fnname2])+')');

    var si = screenIndex;
    if (typeof(window[fnname1])=='function') window[fnname1](screenIndex, 'remove', m);       
    if (typeof(window[fnname2])=='function') window[fnname2](m, 'set', si);       

    screenIndex = m;
}

function FocusToControl__(action, obj)
{
    if (obj==null) { DeviceConsole('FocusToControl__('+action+') => obj null error'); return; }

    var etype = obj.getAttribute('etype');
    var si = GetScreenIndex(obj); // obj.getAttribute('screenindex');
    var x = obj.getAttribute('x');
    var ro = Int($(obj).attr('ro'));
    var ci = Int($(obj).attr('ci'));

    var clsname = 'ppanel' + x + '_active';
    var itemid = x + '_' + ro + '_' + ci;

    DeviceConsole('FocusToControl__('+action+') => '+ etype + ', x: ' + x + ', itemid: ' + itemid);

    if (action=='set') {
        SetScreenIndex( GetScreenIndex(obj) );
    }
    if (etype=='button' || etype=='textbox') 
    {
        if (action=='set') { $(obj).addClass(clsname); obj.focus(); }
        else if (action=='remove') $(obj).removeClass(clsname);
        if (etype=='textbox') Device_ScrollControlAboveKeyboard(obj, action);
    }
    else if (etype=='dropdown')
    {
        var loaded = obj.getAttribute('loaded');
        var expandonfocus = obj.getAttribute('expandonfocus');

        var box = obj.children[0];
        var owlObj = obj.children[1];
        if (action=='set') { 
            //DeviceConsole('dropdown: '+loaded);    
            $(box.children[0]).addClass(clsname); 
            if (loaded==1 && owlObj.style.display=='none' && expandonfocus==1) $(owlObj).show();
            if (owlObj.style.display != 'none') {
                $(owlObj.children[ro]).addClass(clsname); 
                ScrollIntoView2(etype, obj, x, owlObj.children[ro]);
            }
            SetFocusToInfocus(); 
        } 
        else if (action=='remove') {
            if (owlObj.style.display != 'none') $(owlObj.children[ro]).removeClass(clsname); 
            else $(box.children[0]).removeClass(clsname); 
        }
    }
    else if (etype=='search')
    {
        SearchFilterToggle_(obj, action);
        if (action=='set') { $('#searchInput'+x).addClass(clsname);  $('#searchInput'+x).focus(); }
        else if (action=='remove') { $('#searchInput'+x).removeClass(clsname); }
    }
/*    else if (etype=='collist')
    {
        if (action=='set') {
            $('#' + itemid).addClass(clsname); 
            $('#' + itemid).children().each( function() { $(this).addClass(clsname); });
            ScrollIntoView(obj);  
            ExternalPoster(obj);
            SetFocusToInfocus(); 
            if (obj.getAttribute('showitemContents')==1) GetActionUrl(x+'_'+ro+'_'+ci, 'visible');
        }
        else if (action=='remove') {
            $('#' + itemid).removeClass(clsname); 
            $('#' + itemid).children().each( function() { $(this).removeClass(clsname); });
        }
    }*/
    else if (etype=='gridlist')
    {
        if (action=='set') {
            $('#' + itemid).addClass(clsname); 
            Gridlist_ScrollIntoView(obj);  
            ExternalPoster(obj);
            SetFocusToInfocus(); 
            if (obj.getAttribute('showitemContents')==1) GetActionUrl(x+'_'+ro+'_'+ci, 'visible');
        }
        else if (action=='remove') $('#' + itemid).removeClass(clsname); 
    }
    else if (etype=='rowlist')
    {
        if (! $('#R'+x+'_'+ro).hasClass('ppanel' + x + '_row_active')) {
            $('#R'+x+'_'+ro).addClass('ppanel' + x + '_row_active');
            $('#R'+x+'_'+ro+'_box').show();
        }
        if (action=='set') {
            $('#' + itemid).addClass(clsname); 

            ExternalPoster(obj);
            SetFocusToInfocus(); 
            if (obj.getAttribute('showitemContents')==1) GetActionUrl(x+'_'+ro+'_'+ci, 'visible');
        }
        else if (action=='remove') {
            $('#' + itemid).removeClass(clsname); 
        }
    }
    else if (etype=='serverlist' || etype=='connectionlist' || etype=='resumeplay' || etype=='channelgallery' || etype=='postergallery')
    {
        var owlObj = document.getElementById('owl'+x);
        var itemObj = owlObj.children[ro];
        if (action=='set') { 
            $(itemObj).addClass(clsname);
            ScrollIntoView2(etype, obj, x, itemObj);
            SetFocusToInfocus(); 

            if (etype=='serverlist') AutoPlayVideo(obj); 
            else if (etype=='channelgallery' && $(obj).attr('video') > 0) window.setTimeout("ChannelGallery_Play('"+x+"',"+ro+",1)", 1000);
        } 
        else if (action=='remove') {
            $(itemObj).removeClass(clsname);
            if (etype=='postergallery') $('#'+obj.id+'_poster').hide();
        }
    }
    else if (etype=='keyboard')
    {
        if (action=='set') { 
            $('#' + itemid).addClass(clsname); 
            ScrollIntoView(obj); 
            SetFocusToInfocus(); 
        } 
        else if (action=='remove') $('#'+itemid).removeClass(clsname); 
    }
    else if (etype=='episodePanel')
    {
        if (action=='set') { 
            $('#' + itemid).addClass(clsname); 
            VerticalScroll(obj, document.getElementById(itemid), 'owl'+x);
            //ScrollIntoView_episodePanel(obj,itemid);
            SetFocusToInfocus(); 
        } 
        else if (action=='remove') $('#'+itemid).removeClass(clsname); 
    }
    else if (etype=='programlist')
    {
        if (action=='set') { 
            $('#' + x + '_' + ro).addClass(clsname); 
            ProgramList_ScrollRowIntoView(x);
            SetFocusToInfocus(); 
        } 
        else if (action=='remove') $('#' + x + '_' + ro).removeClass(clsname); 
    }
    else if (etype=='nextprevchannel')
    {
        if (action=='set') 
        { 
            var j1 = $(obj).children()[0];  $(j1).addClass(clsname+'_l');
            var j2 = $(obj).children()[1];  $(j2).addClass(clsname+'_r');
        } 
        else if (action=='remove') 
        { 
            var j1 = $(obj).children()[0];  $(j1).removeClass(clsname+'_l'); 
            var j2 = $(obj).children()[1];  $(j2).removeClass(clsname+'_r'); 
        }
    }
    else if (etype=='pguide')
    {
        //if (action=='set') PopulateProgramGuide(x);
        ProgramGuideCell(x, action); 
        SetFocusToInfocus(); 
    }
    else if (etype=='optionsList')
    {
        var ro = obj.getAttribute('ro');
        if (action=='set') { $('#'+x+'_'+ro+'_0').addClass(clsname); ScrollIntoView_optionsList(obj); SetFocusToInfocus(); } 
        else if (action=='remove') $('#'+x+'_'+ro).removeClass(clsname); 
    }
    else if (etype=='calendar') 
    {
        var iscontrol = obj.getAttribute('iscontrol');
        var visibleonfocus = obj.getAttribute('visibleonfocus');
        var ro = obj.getAttribute('ro');
        var ci = obj.getAttribute('ci');
        if (action=='set') 
        { 
            var iid = '#' + x + '_' + ro + '_' + ci;
            ////DeviceConsole(iid);
            if (iscontrol==1 && visibleonfocus==1 && obj.style.display=='none') ShowOptionControl('calendar');
            if (ro==-1 || Int($(iid).html())>0) $(iid).addClass(clsname);  
            SetFocusToInfocus();
        } 
        else if (action=='remove') 
        {
            $('#' + x + '_' + ro + '_' + ci).removeClass(clsname); 
        }
    }
    else if (action=='set') SetFocusToInfocus();  
    DeviceConsole('FocusToControl__ ----------');
}

function ScrollIntoView2(etype, obj, x, itemObj)
{
    if (etype=='connectionlist' || etype=='resumeplay' || etype=='serverlist' || etype=='dropdown') Serverlist_ScrollIntoView(obj, x, itemObj);
    else if (etype=='postergallery' || etype=='channelgallery') PosterGallery_ScrollIntoView(obj, x, itemObj); 
}

function FocusToControl(action)
{
    DeviceConsole('FocusToControl: '+action);
    var obj = GetInputElementByIndex(-1);
    //DeviceConsole(obj);
    if (obj != null) FocusToControl__(action, obj);
}

function FindActiveScreen(lvl)
{
    var fcon=document.getElementById('fcon');
    for(var  i = 0 ; i < fcon.children.length ; i++) {
        if (fcon.children[i].getAttribute('level')!=lvl) continue;
        if (fcon.children[i].style.display=='none') continue;
        return fcon.children[i];
    }
    return null;
}

function UpdateActiveIndex_(ans)
{
    //DeviceConsole('UpdateActiveIndex_: '+ans+', screenIndex: '+screenIndex);
    var scObj = document.getElementById('screen_container'+screenIndex);
    if (scObj==null) return 0;

    var aIStr = scObj.getAttribute('activeIndex');
    var ctrls = scObj.getAttribute('ctrls').split(',');
    var k = ctrls.indexOf(aIStr);

    k += Int(ans);
    if (ctrls[k]=='.') 
    {
        if (ans==1) scObj.setAttribute('activeIndex', ctrls[1]);
        if (ans==-1) scObj.setAttribute('activeIndex', ctrls[ctrls.length-2]);
        FocusToControl('set'); 
        return 1;
    }
    else if (ctrls[k]=='<') { 
        var lvlStr = scObj.parentElement.getAttribute('level');
        var lvl = Int(scObj.parentElement.getAttribute('level'))-1;
        if (lvlStr=='form') lvl = 0;
        var newScr = FindActiveScreen(lvl);
        var v=screenIndex;
        if (newScr != null) {
            var si0 = newScr.children[1].id.substr(16);
            SetScreenIndex(si0);
            if (UpdateActiveIndex_(0)==1) return 1; 
        }
        SetScreenIndex(v);
    }
    else if (ctrls[k]=='>') { 
        var lvl = Int(scObj.parentElement.getAttribute('level'))+1;
        var newScr = FindActiveScreen(lvl);
        var v=screenIndex;
        if (newScr != null) {
            var si0 = newScr.children[1].id.substr(16);
            SetScreenIndex(si0);
            if (UpdateActiveIndex_(0)==1) return 1; 
        }
        SetScreenIndex(v);
    }
    scObj.setAttribute('activeIndex', ctrls[k]); 
    FocusToControl('set'); 
    return 1;
}

function UpdateActiveIndex(ans, panelObj)
{
    //DeviceConsole('UpdateActiveIndex: '+ans+', screenIndex: '+screenIndex);
    if (ans == 0) return 0;
    var scObj = document.getElementById('screen_container'+screenIndex);
    if (scObj==null) return 0;

    var aIStr = scObj.getAttribute('activeIndex');
    return UpdateActiveIndex_(ans);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function UpdateSelectedIndexOfPanel(obj, ro, ci)
{
    //DeviceConsole('UpdateSelectedIndexOfPanel (ro: '+ro+', ci: '+ci+')');
    var etype = obj.getAttribute('etype');
    obj.setAttribute('ro', ro);
    obj.setAttribute('ci', ci);
    if (etype == 'rowlist' /* || etype=='collist' */) {
        var x = obj.getAttribute('x');
        document.getElementById('R'+x+'_'+ro).setAttribute('c', ci);
    }

    SetScreenIndex( GetScreenIndex(obj) ); // Int(obj.getAttribute('screenindex'));
}

function UpdateSelectedIndexOfPanel_(obj, ro, ci)
{
    // remove focus from current subitem of obj
    FocusToControl__('remove', obj);
    UpdateSelectedIndexOfPanel(obj, ro, ci);

    // set focus to new subitem of obj
    FocusToControl__('set', obj);
    return 0;
}

function OnScrollOver()
{
    DeviceConsole('OnScrollOver');
    scrollingEvent.updating = 1;
    var me = document.getElementById(scrollingEvent.me);
    var etype = me.getAttribute('etype');
    var x = me.getAttribute('x');
    if (etype=='rowlist' /*|| etype=='collist'*/)
    {
        var rowGroupObj = me.children[0];  // document.getElementById('rowGroup'+x);  
        var firstObj = rowGroupObj.children[0];  
        var rowHeight = $(firstObj).height();
        var lastVisibleRow = Math.ceil( (me.scrollTop+$(me).height()) / rowHeight );  

        //var roMax = me.getAttribute('romax');
        if (lastVisibleRow >= rowGroupObj.children.length) lastVisibleRow = rowGroupObj.children.length-1;
        for (var k = lastVisibleRow ; k > 0 ; k--) {
            me.setAttribute('ro', k);
            Rowlist_LoadImages(x);
        }
        me.setAttribute('ro', lastVisibleRow);
    }
    scrollingEvent.scrollTimer = null;
    scrollingEvent.updating = 0;
}

function OnScroll(e, me)
{
    //DeviceConsole('OnScroll');

    var etype = me.getAttribute('etype');
    if (etype=='rowlist' /*|| etype=='collist'*/)
    {
        if (scrollingEvent.updating==0) {
            if (scrollingEvent.scrollTimer !== null) window.clearTimeout(scrollingEvent.scrollTimer); 
            scrollingEvent.scrollTimer = window.setTimeout('OnScrollOver()', 700);
            scrollingEvent.me = me.id;
        }
    }
}

function OnFocusEvent(evt, me)
{
    var panelObj = null;
    DeviceConsole('\n----------------------------------\nOnFocusEvent');
    //if (evt != null) { DeviceConsole('event:'); DeviceConsole(evt); }
    clickEventProcessed=0;
    if (myClick[0]==1) { myClick=[0,0,0,'']; return 0; }

    var etype = me.getAttribute('etype');
    //DeviceConsole('OnFocusEvent: '+ etype); 
    //DeviceConsole('me:'); 
    //DeviceConsole(me); 

    if (etype==null) 
    {
        if (evt != null) panelObj = $(evt.target).closest('[etype]')[0];  
        else { var pa = me.id.split('_'); panelObj = document.getElementById('ppanel'+pa[0]);  }
        etype = panelObj.getAttribute('etype'); 
    }
    else panelObj = me;

    DeviceConsole('OnFocusEvent: '+ etype + ', id: ' + me.id); 
    var f = 'OnClick_'+screenIndex;
    //DeviceConsole( f + ' => ' + typeof(window[f])); 

    if (typeof(window[f])=='function') {
        var ans = window[f](evt, panelObj);       
        DeviceConsole( f + ' returned => ' + ans); 
        if (ans != undefined && ans != 0) {
            if (evt != undefined) evt.stopPropagation();
            return 0;
        }
    }

    if (etype=='button')
    {
        if (evt != undefined) { evt.stopPropagation();  me = evt.target; }
        clickEventProcessed=1;

        var newsi = panelObj.getAttribute('x').slice(0, -2);
        SetScreenIndex(newsi);

        if (appVersion >= 112) {
            OnKeyDownEvent__(me, "OK", 1);
        }
        else {
            FocusToControl('remove');
            var ki = me.getAttribute('keyIndex');
            document.getElementById('screen_container'+screenIndex).setAttribute('activeIndex', ki);
            FocusToControl('set');
        }
    }
    else if (etype=='textbox')
    {
        if (evt != undefined) { evt.stopPropagation();  me = evt.target; }
        clickEventProcessed=1;
        if (appVersion >= 112) {
            OnKeyDownEvent__(me, "OK", 1);
        }
        else {
            FocusToControl('remove');
            var ki = me.getAttribute('keyIndex');
            document.getElementById('screen_container'+screenIndex).setAttribute('activeIndex', ki);
            FocusToControl('set');
        }
        var fname = "ppanel" + me.getAttribute('x') + '_clickEvent';
        if (typeof(window[fname])=='function') window[fname]();
    }
    else if (etype=='dropdown')
    {
        if (evt != undefined) evt.stopPropagation();

        var x = panelObj.getAttribute('x');
        var owlObj = document.getElementById('owl'+x);
        if (owlObj.style.display != 'none') {
            FocusToControl__('remove', panelObj);
            var child = evt.target;
            var i = 0;
            while( (child = child.previousSibling) != null ) i++;
            //DeviceConsole('dropdown child index: '+i);
            panelObj.setAttribute('ro', i);
            FocusToControl__('remove', panelObj);
        }

        clickEventProcessed=1;
        OnKeyDownEvent__(panelObj, "OK", 1);
    }
    else if (etype=='keyboard')
    {
        FocusToControl('remove');
        var parts = evt.target.id.split('_');
        me.setAttribute('ro', parts[1]);
        me.setAttribute('ci', parts[2]);

        FocusToControl('set');
        OnKeyDownEvent__(me, "OK", 1);
        return 0;
    }
    else if (etype=='options')
    {
        ShowOptionsList();
        return 0;
    }
    else if (etype=='optionsList')
    {
        clickEventProcessed=1;
        FocusToControl('remove');
        var parts = me.id.split('_');
        var panelObj = document.getElementById('ppanel' + parts[0]);
        panelObj.setAttribute('ro', parts[1]);
        FocusToControl('set');
        OnKeyDownEvent__(panelObj, "OK", 1);
        return 0;
    }
    else if (etype=='episodePanel')
    {
        clickEventProcessed=1;
        FocusToControl('remove');
        var parts = evt.target.id.split('_');
        var panelObj = document.getElementById('ppanel' + parts[0]);
        UpdateSelectedIndexOfPanel_(panelObj, parts[1], parts[2]);
        OnKeyDownEvent__(panelObj, "OK", 1);
        return 0;
    }
    else if (etype=='gridlist' || etype=='poster')
    {
        clickEventProcessed=1;
        FocusToControl('remove');
        //var newsi = panelObj.getAttribute('x').slice(0, -2);
        //SetScreenIndex(newsi);
        //FocusToControl__('remove', panelObj);
        var parts = me.id.split('_');
        while(parts.length < 3) parts.push('0');               // to adjust for var co
        //DeviceConsole('OnFocusEvent: '+etype+', parts: '+ parts[0]+', '+parts[1]+', '+parts[2]);
        var panelObj = document.getElementById('ppanel' + parts[0]);
        UpdateSelectedIndexOfPanel_(panelObj, parts[1], parts[2]);

        var newki = panelObj.getAttribute('keyIndex');
        document.getElementById('screen_container'+screenIndex).setAttribute('activeIndex', newki);
        OnKeyDownEvent__(panelObj, "OK", 1);
    }
    else if (etype=='rowlist')
    {
        var x = panelObj.getAttribute('x');
        var ro = panelObj.getAttribute('ro');
        var ci = panelObj.getAttribute('ci');

        clickEventProcessed=1;
        FocusToControl('remove');
        var newsi = x.slice(0, -2);
        SetScreenIndex(newsi);
        FocusToControl__('remove', panelObj);

        var parts = me.id.split('_');
        while(parts.length < 3) parts.push('0');               

        if (parts[1] != ro) Rowlist_RowChanged(panelObj, x, ro, ci, parts[1]);

        var rowObj = document.getElementById('R'+x+'_'+parts[1]);

        // set new column
        panelObj.setAttribute('ci', parts[2]);
        rowObj.setAttribute('c', parts[2]);
        Rowlist_ScrollColumnIntoView(panelObj, x, parts[1], parts[2]);
        FocusToControl__('set', panelObj);

        //UpdateSelectedIndexOfPanel_(panelObj, parts[1], parts[2]);
        //var newki = panelObj.getAttribute('keyIndex');
        //document.getElementById('screen_container'+screenIndex).setAttribute('activeIndex', newki);
        OnKeyDownEvent__(panelObj, "OK", 1);
    }
    else if (etype=='resumeplay' || etype=='connectionlist' || etype=='serverlist')
    {
        clickEventProcessed=1;
        FocusToControl__('remove', panelObj);

        var x = $(panelObj).attr('x');
        var owlObj = document.getElementById('owl' + x);
        var ro = Array.prototype.indexOf.call(owlObj.children, evt.target);
        UpdateSelectedIndexOfPanel_(panelObj, ro, 0);

        var newki = panelObj.getAttribute('keyIndex');
        document.getElementById('screen_container'+screenIndex).setAttribute('activeIndex', newki);
        OnKeyDownEvent__(panelObj, "OK", 1);
    }
    else if (etype=='postergallery')
    {
        if (evt != undefined) evt.stopPropagation();

        var x = panelObj.getAttribute('x');
        var owlObj = document.getElementById('owl'+x);
        FocusToControl__('remove', panelObj);
        var child = evt.target;
        var i = 0;
        while( (child = child.previousSibling) != null ) i++;
        panelObj.setAttribute('ro', i);
        //DeviceConsole('OnFocusKey=>postergallery=>ro: '+i);
        clickEventProcessed=1;
        FocusToControl__('set', panelObj);
    }
}

//------------------------------------------------------------------------------
function GetNextTabControl(myIndex)
{
    var ni = Int(myIndex) + 1;
    var frame = GetInputElementByIndex(ni);
    if (frame != null) {
        if (frame.hasAttribute('etype') && frame.getAttribute('etype') == 'tabpage') return frame;
    }
    return null;
}

//==============================================================================
function VerticalScroll(container, child, sliderID)
{
    DeviceConsole('VerticalScroll');
    var containerRect = container.getBoundingClientRect();
    if (child==null) return;

    var childRect = child.getBoundingClientRect();
    var yMove = 0;

    if (childRect.top < containerRect.top) yMove = containerRect.top - childRect.top; 
    else if (childRect.bottom > containerRect.bottom) yMove = containerRect.bottom - childRect.bottom; 
    if (Math.abs(yMove) > 0)
    {
        if (DEVICEID==11 || DEVICEID==4 || DEVICEID==1) {
            var mlX = container.scrollTop;
            var xx = mlX - yMove;
            container.scrollTo(0, xx);
        }
        else {
            var mlX = Int($('#'+sliderID).css('margin-top'));
            $('#'+sliderID).animate({ marginTop: mlX + yMove }, 100);
        }
        return 1;
    }
    return 0;
}

function HorizontalScroll(container, child, sliderID)
{
    var containerRect = container.getBoundingClientRect();
    var childRect = child.getBoundingClientRect();
    var xMove = 0;

    if (childRect.left < containerRect.left) xMove = containerRect.left - childRect.left; 
    else if (childRect.right > containerRect.right) xMove = containerRect.right - childRect.right; 
    if (Math.abs(xMove) > 0)
    {
        if (DEVICEID==11 || DEVICEID==4) {
            var mlX = container.scrollLeft;
            var xx = mlX - xMove;
            container.scrollTo(xx, 0);
        }
        else {
            var mlX = Int($('#'+sliderID).css('margin-left'));
            $('#'+sliderID).animate({ marginLeft: mlX + xMove }, 100);
        }

        return 1;
    }
    return 0;
}

function Rowlist_Focus(panelObj, status)
{
    var x = panelObj.getAttribute('x');
    var ro = panelObj.getAttribute('ro');
    var ci = panelObj.getAttribute('ci');

    if (status=='set') {
        Rowlist_ScrollRowIntoView(panelObj, x, ro);
        $('#R'+x+'_'+ro).addClass('ppanel' + x + '_row_active'); 
        $('#R'+x+'_'+ro+'_box').show();

        // set new column
        Rowlist_ScrollColumnIntoView(panelObj, x, ro, ci);
        FocusToControl__('set', panelObj);
    }
    else if (status=='remove') {
        FocusToControl__('remove', panelObj);
        $('#R'+x+'_'+ro).removeClass('ppanel' + x + '_row_active');
        $('#R'+x+'_'+ro+'_box').hide();
    }
}

function Rowlist_RowChanged(panelObj, x, ro, ci, newro)
{
    var xro = $('#R'+x+'_'+newro);
    var newci = GetVisibleItem(x+'_'+newro+'_',  Int(xro.attr('c')),  1,  Int(xro.attr('last')));

    Rowlist_Focus(panelObj, 'remove');
    // --------------------------------------------------------------------- 
    // set new row 
    panelObj.setAttribute('ro', newro);
    panelObj.setAttribute('ci', newci);

    Rowlist_Focus(panelObj, 'set');
}

function Rowlist_ScrollRowIntoView(panelObj, x, ro)
{
    DeviceConsole('Rowlist_ScrollRowIntoView');
    var yMove = 0;
    if (panelObj.getAttribute('romax') == 0) return 0;

    var rowObj = document.getElementById('R'+x+'_'+ro);
    if ( rowObj == null) return 0;

    var sbox = panelObj.getAttribute('sbox');
    var prect = panelObj.getBoundingClientRect();
    var rrect = rowObj.getBoundingClientRect();

    if (sbox=='F') yMove = prect.top - rrect.top;
    else {
        if (rrect.top < prect.top) yMove = prect.top - rrect.top;
        else if (rrect.bottom > prect.bottom) yMove = prect.bottom - rrect.bottom;
    }
    if (Math.abs(yMove) > 0)
    {
        if (DEVICEID==11 || DEVICEID==4) {
            var mlX = panelObj.scrollTop;
            var xx = mlX - yMove;
            panelObj.scrollTo(0, xx);
            Rowlist_LoadImages(x);
        }
        else {
            var mlY = Int($('#rowGroup'+x).css('margin-top'));
            $('#rowGroup'+x).animate({ marginTop: mlY + yMove }, 400, function() { Rowlist_LoadImages(x); });
        }
    }
    return 1;
}

function Rowlist_ScrollColumnIntoView(panelObj, x, ro, ci)
{
    DeviceConsole('Rowlist_ScrollColumnIntoView: ' + x+'_'+ro+'_'+ci);
    var xMove = 0;
    var posterObj = document.getElementById(x+'_'+ro+'_'+ci);
    if (posterObj == null) return 0;

    var sbox = panelObj.getAttribute('sbox');
    var owlObj = document.getElementById('owl'+x+'_'+ro);

    if (sbox=='F') {
        var iw = Double(panelObj.getAttribute('iwidth'));
        var iwu = panelObj.getAttribute('iunit');
        xMove = iw * ci;
        if (iwu=='vw') xMove *= ($(window).width() / 100); 
        $(owlObj.parentNode).animate({ scrollLeft:xMove }, 200, function() { Rowlist_LoadRowImages(x, ro); });
    }
    else {
        var postrect = posterObj.getBoundingClientRect();
        var prect = panelObj.getBoundingClientRect();

        if (postrect.left < prect.left) xMove = prect.left - postrect.left;
        else if (postrect.right > prect.right) xMove = prect.right - postrect.right;

        if (Math.abs(xMove) > 0)
        {
            if (DEVICEID==11 || DEVICEID==4) {
                var mlX = owlObj.parentNode.scrollLeft;
                var xx = mlX - xMove;
                //owlObj.parentNode.scrollTo(xx, 0);
                $(owlObj.parentNode).animate({ scrollLeft: xx  }, 200, function() { Rowlist_LoadRowImages(x, ro); });
                Rowlist_LoadRowImages(x, ro);
            }
            else {
                var mlX = Int($(owlObj).css('margin-left'));
                $(owlObj).animate({ marginLeft: mlX + xMove }, 100, function() { Rowlist_LoadRowImages(x, ro); });
            }
        }
    }

    // If counter is displayed update it
    var counterObj = document.getElementById(x+'_'+ro+'_counter');
    if (counterObj != null) {
        counterObj.innerHTML = (Int(ci)+1) + '/' + counterObj.getAttribute('counter');
    }
}

function Gridlist_GetType(panelObj)
{
    var x = panelObj.getAttribute('x');
    var owlObj = document.getElementById('owl'+x);
    var child0 = owlObj.children[0];
    var cmax = Math.floor($(panelObj).width() / $(child0).outerWidth());
    if (cmax==1) return 'vert';
    return 'horz';
}

function Gridlist_ScrollIntoView(panelObj)
{
    //DeviceConsole('Gridlist_ScrollIntoView');
    var x = panelObj.getAttribute('x');
    var ro = panelObj.getAttribute('ro');
    var ci = panelObj.getAttribute('ci');
    var itemObj = document.getElementById(x+'_'+ro+'_'+ci)

    var type = Gridlist_GetType(panelObj);
    //DeviceConsole('gridlist_getType: '+type);

    if (panelObj.getAttribute('romax') != 1) 
        VerticalScroll(panelObj, itemObj, 'owl'+x);

    //==========================================================================
    if (panelObj.getAttribute('cimax') > 1) 
        HorizontalScroll(panelObj, itemObj, 'owl'+x);

    //--------------------------------------------------------------------------
    var externalPosterObj = document.getElementById(panelObj.id + '_poster');
    if (externalPosterObj != null) externalPosterObj.src = document.getElementById(posterID).getAttribute('data');
}

function PosterGallery_ScrollIntoView(panelObj, x, itemObj)
{
    if (DEVICEID != 4) 
    {
        if (panelObj.getAttribute('type') == '0') {
            var yMove = Double( panelObj.scrollTop ) + $(itemObj).position().top;
            panelObj.scrollTo(0, yMove);
        }
        else 
        {
            var xMove = Double( panelObj.scrollLeft ) + $(itemObj).position().left;
            panelObj.scrollTo(xMove, 0);
        }
    }

    //--------------------------------------------------------------------------
    var externalPosterObj = document.getElementById(panelObj.id + '_poster');
    if (externalPosterObj != null) {
        $(externalPosterObj).show();
        externalPosterObj.children[0].src = itemObj.src;
    }
}

function ScrollIntoView_collist(panelObj)
{
    //DeviceConsole('ScrollIntoView_collist');
    var x = panelObj.getAttribute('x');
    var ro = panelObj.getAttribute('ro');
    var ci = panelObj.getAttribute('ci');
    var posterID = x + '_' + ro + '_' + ci;
    var r0 = 'R' + x + '_' + ro;

    var psz = [ $(panelObj).offset().left, $(panelObj).width(), $(panelObj).offset().top, $(panelObj).height()];
    var rsz = [ $('#'+posterID).offset().left, $('#'+posterID).width(), $('#'+posterID).offset().top, $('#'+posterID).height()];

    var moved=0; 
    var yMove = 0;
    if (rsz[2] < psz[2]) yMove = psz[2] - rsz[2]; 
    else if (rsz[2]+rsz[3] > psz[2]+psz[3]) yMove = psz[2]+psz[3] - (rsz[2]+rsz[3]);

    if (Math.abs(yMove) > 0)
    {
        var mlY = Int($('#rowGroup'+x).css('margin-top'));
        $('#rowGroup'+x).animate({ marginTop: mlY + yMove }, 100,
                                   function() { Rowlist_LoadImages(x); } 
                                );
        moved=1;
    }
    // If counter is displayed update it
    var counterObj = document.getElementById(x+'_'+ro+'_counter');
    if (counterObj != null) {
        counterObj.innerHTML = (Int(ci)+1) + '/' + counterObj.getAttribute('counter');
    }

    //--------------------------------------------------------------------------
    var externalPosterObj = document.getElementById(panelObj.id + '_poster');
    if (externalPosterObj != null) externalPosterObj.src = document.getElementById(posterID).getAttribute('data');

    if (moved==1) Rowlist_LoadImages(x);

}

function Serverlist_ScrollIntoView(panelObj, x, itemObj)
{
    if (panelObj.getAttribute('type') == '0') VerticalScroll(panelObj, itemObj, 'owl'+x);
    else HorizontalScroll(panelObj, itemObj, 'owl'+x);
}

function ScrollIntoView_gridlist(panelObj)
{
    var etype = panelObj.getAttribute('etype');
    var x = panelObj.getAttribute('x');
    var ro = panelObj.getAttribute('ro');
    var ci = panelObj.getAttribute('ci');
    var posterID = x + '_' + ro + '_' + ci;

    var vs=1;
    if (vs==1) VerticalScroll(panelObj, document.getElementById(posterID), 'owl'+x);
    if (etype=='connectionlist') return;

    //==========================================================================
    vs=1;
    HorizontalScroll(panelObj, document.getElementById(posterID), 'owl'+x);
    //--------------------------------------------------------------------------
    var externalPosterObj = document.getElementById(panelObj.id + '_poster');
    if (externalPosterObj != null) externalPosterObj.src = document.getElementById(posterID).getAttribute('data');
}

function ScrollIntoView_optionsList(panelObj)
{
    var etype = panelObj.getAttribute('etype');
    var x = panelObj.getAttribute('x');
    var ro = panelObj.getAttribute('ro');
    var ci = panelObj.getAttribute('ci');
    var posterID = x + '_' + ro;

    var panelObj1 = panelObj.children[0];
    var psz = [ $(panelObj1).offset().left, $(panelObj1).width(), $(panelObj1).offset().top, $(panelObj1).height()];
    var isz = [ $('#'+posterID).offset().left, $('#'+posterID).width(), $('#'+posterID).offset().top, $('#'+posterID).height()];

    var yMove = 0;
    if (isz[2] < psz[2]) yMove = psz[2] - isz[2];
    else if (isz[2]+isz[3] > psz[2]+psz[3]) yMove = psz[2]+psz[3] - (isz[2]+isz[3]);
    if (Math.abs(yMove) > 0)
    {
        var mlY = Int($('#owl'+x).css('margin-top'));
        $('#owl'+x).animate({ marginTop: mlY + yMove }, 100);
    }
}

function ScrollIntoView(panelObj)
{
    var etype = panelObj.getAttribute('etype');
    //DeviceConsole("ScrollIntoView : " + etype);
    // scroll view if required
    if (etype=='connectionlist') ScrollIntoView_gridlist(panelObj);
    else if (etype == 'gridlist') Gridlist_ScrollIntoView(panelObj);
    //else if (etype == 'collist') ScrollIntoView_collist(panelObj);
    else if (etype=='optionsList') ScrollIntoView_optionsList(panelObj);
}

function CreateDivTabView(url, divid)
{
    var htmlString = "<div id='"+divid+"' etype='tab' ";
    htmlString += "style='background-color:transparent;position:absolute;left:0px;top:0px;width:100%;height:100%;pointer-events: none;' ";
    htmlString += "src='"+url+"' history=''></div>";
    var div = document.createElement('div');
    div.innerHTML = htmlString;
    return div.firstChild; 
}

function GetVisibleItem2(parentObj, ro, direction)
{
    var notvisible=1;
    var v = Int(ro);
    //DeviceConsole('GetVisibleItem xro=> '+xro+', direction=> '+direction+', from=>'+from);
    while(notvisible==1)
    {
        v += direction;
        if (v <= -1) return -1;
        if (v >= parentObj.children.length) return LASTITEM;
        if (parentObj.children[v].style.display != 'none') return v;
    }
    return v;
}

function GetVisibleItemR(x, ro, ci, direction)
{
    var notvisible=1;
    var v = Int(ro);
    //DeviceConsole('GetVisibleItem xro=> '+xro+', direction=> '+direction+', from=>'+from);
    while(notvisible==1)
    {
        v += direction;
        if (v <= -1) return -1;

        var pobj = document.getElementById(x+'_'+v+'_'+ci);
        if (pobj==null) return LASTITEM;
        if (pobj.style.display != 'none') return v;
    }
    return v;
}

function GetVisibleItem(xro, from, direction, cimax)
{
    var notvisible=1;
    var c = Int(from);
    //DeviceConsole('GetVisibleItem xro=> '+xro+', direction=> '+direction+', from=>'+from+');
    while(notvisible==1)
    {
        var pobj = document.getElementById(xro+c);
        if (pobj!=null && pobj.style.display != 'none') return c;
        c += direction;
        if (direction==-1 && c <= -1) return -1;
        if (direction==1 && c >= cimax) return cimax;
    }
    return c;
}

function GetNextPrevPosterRow(panelObj, from, direction)
{
    //DeviceConsole('GetNextPrevPosterRow => from: ' + from + ', direction: ' + direction);
    var etype = panelObj.getAttribute('etype');
    var x = panelObj.getAttribute('x');
    var romax = Int(panelObj.getAttribute('romax'));
    var xro = 'R' + x + '_';

    if (direction==1) return GetVisibleItem(xro, from+1, 1, romax);
    else if (direction==-1) return GetVisibleItem(xro, from-1, -1, romax);
}

//------------------------------------------------------------------------------
function Favorite(cid, obj)
{
    var tz = obj.getAttribute('tz').split('^');
    if (tz[0]=='addF') {
        var panelObj = $(obj).closest('[etype]')[0];  
        var isbinge = panelObj.getAttribute('isbinge');
        var bingeid = '';
        if (isbinge==1) bingeid = panelObj.getAttribute('vid'); 

        var response = AddToFavorite(cid, isbinge, bingeid).split('^'); 
        if (response[0] > 0) {
            ShowAlert('Favorite', response[1], 300);
            obj.innerHTML = 'Remove from Favorite';
            obj.setAttribute('tz', 'removeF^' + response[0]);
        }
    }
    else if (tz[0]=='removeF') {
        Favorite_Remove(tz[1], obj, 'addF');

        var response = RemoveFromFavorite(tz[1]).split('^'); 
        if (response[0] == 1) {
            ShowAlert('Favorite', response[1], 300);
            obj.innerHTML = 'Add to Favorite';
            obj.setAttribute('tz', 'addF^0');
        }
    }
}

function Favorite_Remove(cid, obj, tzAttr)
{
    var response = RemoveFromFavorite(cid).split('^'); 
    if (response[0] == 1) {
        ShowAlert('Favorite', response[1], 300);
        obj.innerHTML = 'Add to Favorite';
        obj.setAttribute('tz', tzAttr + '^0');
    }
}

function GetActionUrl(posterID, screenLoadMethod)
{
    var parts = posterID.split('_');
    var panelObj = document.getElementById('ppanel' + parts[0]);
    var etype = panelObj.getAttribute('etype');

    //DeviceConsole('GetActionUrl (etype: ' + etype + ', posterID: ' + posterID + ', screenLoadMethod: ' +screenLoadMethod);
    var obj = document.getElementById(posterID);
    var cid = $('#'+posterID).attr('cid');
    
    if (etype=='episodePanel') { 
        WebtvVideoX('episode', panelObj, $(obj).html()); 
        return 13; 
    }
    else if (etype=='connectionlist') 
    { 
        var ro = panelObj.getAttribute('ro');
        obj = document.getElementById('owl'+parts[0]).children[ro];

        var response = AjaxCallWaitForResponse('&action=freeThisConnection&cid='+obj.getAttribute('cid') + '&x=' + parts[0] + GetPotionK(), '');   
        $(obj).hide();
        $('#p'+parts[0]).attr('ro', '0');
        return 13; 
    }
    else if (etype=='serverlist') 
    {
        var ro = panelObj.getAttribute('ro');
        obj = document.getElementById('owl'+parts[0]).children[ro];
        var ctype = obj.getAttribute('ctype');
        if (ctype=='video') WebtvVideo('&ctype=video&offset=0&id='+$(panelObj).attr('cid')+'&tz='+obj.getAttribute('tz')); 
        else if (ctype=='binge') WebtvVideo('&ctype=binge&offset=0&tz='+obj.getAttribute('tz')+'&id='); 
        else if (ctype=='resume') {
            if (obj.getAttribute('isbinge')==1) WebtvVideo('&ctype=binge&offset=1&id='+$(panelObj).attr('cid')+'&tz='+obj.getAttribute('tz'));
            else WebtvVideo('&ctype=video&offset=1&id='+$(panelObj).attr('cid')+'&tz='+obj.getAttribute('tz'));
        }
        else if (ctype=='favorite') Favorite($(panelObj).attr('cid'), obj);        
        return 13;
    }
    else if (etype=='optionsList') 
    {
        var epno = obj.getAttribute('epno');
        var tz = obj.getAttribute('tz').split('^');
        var panelObj = document.getElementById('ppanel'+obj.getAttribute('x'));

        if (epno==41) { GotoHomeScreen(); return 13; }
        else if (epno==44) { Favorite(cid, obj); return 13; } 
        else if (epno==46) { ShowOptionControl('calendar'); return 13; } 
        else if (epno==47) { ShowOptionControl('search'); return 13; } 
        else if (epno==43) { logout(); return 13; } 
    }
    else if (etype=='channelgallery') 
    {
        WebTVDetailsReplace(panelObj, cid);
        return 13;
    }
    else if (etype=='dropdown') 
    {
        FocusToControl__('remove', panelObj);
        var ro = panelObj.getAttribute('ro');
        obj = document.getElementById('owl'+parts[0]).children[ro];
        var ctype = $(panelObj).attr('ctype');
        var cid = $(obj).attr('cid');
        if (ctype==1) { 
            var lvlObj = FindParentHavingAttribute(panelObj, 'level');
            if (lvlObj == null) WebTVBrowse(cid, parts[0], screenLoadMethod);  //this condition will never arise
            else WebTVBrowse(cid, Int(lvlObj.getAttribute('level'))+1, screenLoadMethod); 
            return 13; 
        }
    }

    //--------------------------------------------------------------------------
    var ctype = $('#'+posterID).attr('ctype');
    //DeviceConsole('ctype=' + ctype + ', cid=' + cid);
    if (ctype==1) { 
        
        var lvlObj = FindParentHavingAttribute(panelObj, 'level');
        //DeviceConsole(lvlObj);
        if (lvlObj == null) WebTVBrowse(cid, parts[0], screenLoadMethod);  //this condition will never arise
        else WebTVBrowse(cid, Int(lvlObj.getAttribute('level'))+1, screenLoadMethod); 
        return 13; 
    }
    else if (ctype==2) WebTVDetails(cid+'&ctype=2&parent='+parts[0], screenLoadMethod);  
    else if (ctype==3) WebTVDetails(cid+'&ctype=3&parent='+parts[0], screenLoadMethod);  
    else if (ctype=='video') WebtvVideoX('video', panelObj, obj.getAttribute('tz'));
    else if (ctype=='favorite') Favorite(cid, obj);        
    else if (ctype=='showRatingScreen') WebTVForm('&formType=starrating&layoutName='+obj.getAttribute('tz')+'&cid='+cid , 'active', 0);
    return '';
}

//------------------------------------------------------------------------------
function GetAttachedToObject(panelObj)
{
    var attached2 = panelObj.getAttribute('attachedto');    
    if (attached2 == '') return null;

    var ai = attached2.split('~');
    var newObj = GetInputElementByEType(ai[0], Int(ai[1]));  
    return newObj;
}

function ShowOptionControl(ctrlName, refid)
{
    //DeviceConsole('ShowOptionControl: ' + ctrlName + ', refid: ' +refid);
    var optObj = GetInputElementByEType(ctrlName);   
    if (optObj==null) return 0;

    if (ctrlName=='calendar') 
    {
        var newObj = GetAttachedToObject(optObj);
        if (newObj != null)
        {
            if (newObj.getAttribute('etype')=='pguide') PopulateCalendar(optObj.getAttribute('x'), newObj.getAttribute('timestamp'));
        }
    }
    else if (ctrlName=='keyboard') 
    {   
        var x = optObj.getAttribute('x');
        var kbtype = optObj.getAttribute('kbtype');
        var parts = refid.split('~');
        var inputObj = document.getElementById('ppanel'+parts[1]);
        if (parts[0]=='search') inputObj = document.getElementById('searchInput'+parts[1]);

        document.getElementById('kbTitle'+x).innerHTML = inputObj.getAttribute('kbtitle');

        var txtObj = document.getElementById('kbText'+x);
        txtObj.setAttribute('text', inputObj.value);

        var hiddenkeys=0; 
        if (kbtype == 'qwerty') 
        {
            if (inputObj.type=='password') { optObj.setAttribute('password', '1'); $('#'+x+'_4_5').show();   $('#'+x+'_4_5').addClass('shift1_'+x); }
            else { optObj.setAttribute('password', '0'); $('#'+x+'_4_5').hide(); $('#'+x+'_4_5').removeClass('shift1_'+x); hiddenkeys++; }

            if (parts[0]=='textbox') { hiddenkeys++; $('#'+x+'_4_4').hide(); } else $('#'+x+'_4_4').show();
            $('#R'+x+'_4').attr('hiddenkeys', hiddenkeys);
        }

        optObj.setAttribute('attachedto', refid);
        KBRenderText(x);

        optObj.style.left = (($(window).width() - $(optObj).width()) / 2) + 'px';
        optObj.style.top = (($(window).height() - $(optObj).height()) / 2) + 'px';
    }
    Show(optObj.id);
    SetActiveElementByIndex(optObj.getAttribute('keyindex'));
}

function ShowOptionsList()
{
    var optionlistObj = GetInputElementByEType('optionsList');   // GetInputElementByIndex(110);
    if (optionlistObj==null) return 0;
    //DeviceConsole([optionlistObj.style.display, optionlistObj.id]);
    if (optionlistObj.style.display=='block') return 0;
    SetLastIndex(-1);

    Show(optionlistObj.id);
    SetActiveElementByIndex(optionlistObj.getAttribute('keyindex'));
}

function AlertBoxEvent(keycode)
{
    //DeviceConsole('AlertBoxEvent  keycode: '+keycode);
    if (appVersion >= 120) {
        if ($('#myDialogContainer').is(':visible') == true) 
        {
            var btnset = document.getElementById('webtvDBButtonBox');  
            var changed = 0;
            var c = Int($('#myDialogContainer').attr('c'));
            if (keycode=='OK') {
                $('#myDialogContainer').hide();
                btnset.children[c].click();
                FocusToControl('set');
                return 1;
            }
            else if (keycode=='left') { c--; if (c < 0) c=btnset.children.length-1; $('#myDialogContainer').attr('c', c); changed=1; }
            else if (keycode=='right') { c++; if (c >= btnset.children.length) c=0; $('#myDialogContainer').attr('c', c); changed=1; }
            else if (keycode=='back') return 1; 

            if (changed==1) 
            {
                for (var i = 0 ; i < btnset.children.length ; i++) {
                    if (i==c) $(btnset.children[i]).addClass('webtvDBButtonA'); 
                    else $(btnset.children[i]).removeClass('webtvDBButtonA');
                }
                return 1;
            }
        }
        return 0;
    }
    
    if (document.getElementById('fcon').getAttribute('alert')=='1') {
        var btnset = $('.ui-dialog-buttonset')[0];
        var bc = Int( $('#dialogContainer').attr('bc') );
        var c = Int( $('#dialogContainer').attr('c') );
        var changed=0;
        //DeviceConsole('AlertBoxEvent  keycode: '+keycode+', bc: '+bc+', c: '+c);

        if (keycode=='OK') btnset.children[c].click();
        else if (keycode=='left') { c--; if (c < 0) c=bc-1; $('#dialogContainer').attr('c', c); changed=1; }
        else if (keycode=='right') { c++; if (c >= bc) c=0; $('#dialogContainer').attr('c', c); changed=1; }
        if (changed==1) 
        {
            for (var i = 0 ; i < bc ; i++) {
                if (i==c) $(btnset.children[i]).addClass('ui-state-hover'); 
                else $(btnset.children[i]).removeClass('ui-state-hover');
            }
        }
        return 1;
    }
    return 0;
}

function OnKeyDownEvent2(keycode)
{
    DeviceConsole('OnKeyDownEvent2: '+keycode+', screenIndex: '+screenIndex);

    if (AlertBoxEvent(keycode)==1) return;
    if (document.getElementById('fcon').getAttribute('alert')=='1')
    {
        var btnset = $('.ui-dialog-buttonset')[0];
        btnset.children[0].click();
        return;
    }
    if (typeof(screenIndex)=='undefined' || screenIndex==-1) 
    { 
        var fcon = document.getElementById('fcon');
        if (fcon.children[0].id == 'banner') {
            CloseBanner(); 
            return; 
        }
    }

    var f = 'EAOnKeyDown_'+screenIndex;
    DeviceConsole( f + ' => ' + typeof(window[f])); 
    if (typeof(window[f])=='function') {
        var ans = window[f](keycode, screenIndex);       
        //DeviceConsole( f + ' returned => ' + ans); 
        if (ans != undefined && ans != 0) return 0;
    }

    if (HotKey(null, keycode)=='actioncomplete') return 0; 
    
    var panelObj = GetInputElementByIndex(-1);
    if (panelObj == null) return 0;
    var ans = OnKeyDownEvent__(panelObj, keycode, 0);
    DeviceConsole('OnKeyDownEvent2: -------------');
    return ans;
}

function OnKeyDownEvent(e)
{
    //DeviceConsole('OnKeyDownEvent');
    var keycode = Device_GetKeyCode(e);
    if (keycode=='') return; 

    e.preventDefault();
    PassKeyEvent(keycode);
}

function NextPrev(next, prev, keycode)
{
    if (next != null && next.indexOf(keycode) != -1) return 1;
    if (prev != null && prev.indexOf(keycode) != -1) return -1;
    return 0;
}

function KeyCodeOverride(panelObj, keycode)
{
    var ok = panelObj.getAttribute('ok');
    if (ok != null && ok.indexOf(keycode) != -1) return 'OK';

    var back = panelObj.getAttribute('back');
    if (back != null && back.indexOf(keycode) != -1) return 'back';

    return keycode;
}

function UpKey(panelObj, x, etype, tabNavigationControl)
{
    //DeviceConsole('UpKey x=>'+x+', etype=>'+etype+', tabNavigationControl=>'+tabNavigationControl);
    var ans = ['', 0, 0, 0];  // state [1:processed, 0:Not processed, -1:control not found], ro, ci
                       
    var ro = Int(panelObj.getAttribute('ro'));
    var ci = Int(panelObj.getAttribute('ci'));
    //var romax = Int(panelObj.getAttribute('romax'));

    if (etype=='button' || etype=='textbox')
    {
    	if (tabNavigationControl!=0) return ['updateactiveindex', 0, 0, 0];
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='search') return ['updateactiveindex', 0, 0, 0];
    else if (etype=='dropdown') 
    {
        var owlObj = panelObj.children[1];
        if (owlObj.style.display=='none') 
        {
            if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj);  return ['updateactiveindex', ro, ci, 0]; }
        }
        else 
        {
            var ronew = GetVisibleItem2(owlObj, ro, -1);
            if (ronew > -1) { 
                FocusToControl__('remove', panelObj);
                panelObj.setAttribute('ro', ronew);
                FocusToControl__('set', panelObj);
            }
        }
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='nextprevchannel') 
    {
        if (panelObj.getAttribute('keys')==1) { MoveToChannel(1); return ['actionkey', ro, ci, 0]; }
        return ['updateactiveindex', ro, ci, 0];
    }
    else if (etype=='optionsList') 
    {
        var t = Int(panelObj.getAttribute('type'));
        if (t==0)
        {
            var romax = Int(panelObj.getAttribute('romax'));
            FocusToControl__('remove', panelObj);
            if (ro==0) { 
                if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0]; 
                ro = romax-1;  
            }
            else ro--;

            UpdateSelectedIndexOfPanel(panelObj, ro, ci);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
            //return ['actionkey', ro, ci, 0]; 
        }
        return ['updateactiveindex', ro, ci, 0]; 
    }
    else if (etype=='resumeplay' || etype=='connectionlist' || etype=='serverlist' || etype=='postergallery' || etype=='channelgallery') 
    {
        var t = Int(panelObj.getAttribute('type'));
        if (t==0)
        {
            var owlObj = document.getElementById('owl'+x);
            var ronew = GetVisibleItem2(owlObj, ro, -1);
            if (ronew==-1) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj);  return ['updateactiveindex', 0, 0, 0];  } 
                if ($(panelObj).attr('loop')==0) return ['actioncomplete', 0, 0, 0]; 
                ronew = GetVisibleItem2(owlObj, owlObj.children.length, -1);
            }
            FocusToControl__('remove', panelObj);
            panelObj.setAttribute('ro', ronew);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        else if (t==1 && tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
    }
    else if (etype=='gridlist') 
    {
        var owlObj = document.getElementById('owl'+x);
        var child0 = owlObj.children[0];
        var cmax = Math.floor($(panelObj).width() / $(child0).outerWidth());
        var ro_max = owlObj.children.length;
        if (cmax==1) {
            var newro = GetVisibleItemR(x, ro, '0', -1);
            if (newro==-1) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                newro = GetVisibleItemR(x, ro_max, '0', -1);
            }
        }
        else {
            var newro = GetVisibleItemR(x, ro-cmax+1, '0', -1);
            if (newro==-1) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                newro = GetVisibleItemR(x, -1, '0', 1);
            }
        }
        FocusToControl__('remove', panelObj);
        // set new column
        panelObj.setAttribute('ro', newro);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='rowlist')  
    {
        var xro = 'R' + x + '_';
        var ro_max = panelObj.children[0].children.length;  //getAttribute('romax'));

        var newro = GetVisibleItem(xro, ro-1, -1, ro_max);
        if (newro==-1) 
        { 
            if (tabNavigationControl!=0) {
                Rowlist_Focus(panelObj, 'remove');
                return ['updateactiveindex', ro, ci, 0];
            }
            newro = GetVisibleItem(xro, ro_max-1, -1, ro_max);
        }
        Rowlist_RowChanged(panelObj, x, ro, ci, newro);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='episodePanel') 
    {
        var owlObj =  document.getElementById('owl'+x);
        var cimax = owlObj.children.length;
        var ro_max = Math.floor( $(owlObj).width() / $(owlObj.children[0]).outerWidth(true) );
        if (ci-ro_max < 0) { 
            if (tabNavigationControl!=0) return ['updateactiveindex', 0, 0, 0];  
            var x0 = ci % ro_max; 
            var y0 = Math.floor(cimax / ro_max); 
            ci = y0 * ro_max + x0;
            if (ci > cimax) ci = cimax; 
        }
        else ci -= ro_max;

        FocusToControl__('remove', panelObj);
        panelObj.setAttribute('ci', ci);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='pguide') 
    {
        var romax = Int(panelObj.getAttribute('romax'));
        var r = ro;
        var tms1 = PGSlot2Time(x)
        SetScroll2time(x, 'time:'+tms1);
        ProgramGuideCell(x, 'remove'); 

        for (r-- ; r >= 1 ; r--) {
            if (document.getElementById('pg' + x + '_' + r).style.display != 'none') break;
        } 

        //DeviceConsole(r + '  , ' + tabNavigationControl);
        if (r <= 0) { 
            if (tabNavigationControl!=0) return ['updateactiveindex', r, ci, 0]; 
            for (r = romax ; r > ro ; r--) {
                if (document.getElementById('pg' + x + '_' + r).style.display != 'none') break;
            } 
        }
        panelObj.setAttribute('ro', r);
        ProgramGuide_ScrollRowIntoView(x);

        ci = PGTime2Slot(x, r, tms1 );
        panelObj.setAttribute('ci', ci);
        ProgramGuideCell(x, 'set'); 

        return ['actioncomplete',0,0,0]; 
    }
    else if (etype=='keyboard') 
    {
        if (ro > 0) {
            ro--; 
            var cimax = document.getElementById('R'+x+'_'+ro).children.length - Int(document.getElementById('R'+x+'_'+ro).getAttribute('hiddenkeys'));
            if (ci >= cimax) ci = cimax-1;
        }
        return ['actionkey', ro, ci, 0]; 
    }
    else if (etype=='calendar') 
    {
        if (ro > -1) {
            ro--; 
            var calObj = document.getElementById(x+'_'+ro+'_'+ci);      
            if (calObj != null && Int(calObj.innerHTML)==0) ro=-1; 
            if (ro==-1) ci=1; 
        }
        else 
        {
            var yr = Int(panelObj.getAttribute('year'));
            var mn = Int(panelObj.getAttribute('month'));
            if (ci==1) 
            { 
                yr++; 
                var ymd = CalenderMaxDate(x, yr, mn, 1);
                var cdt = new Date(ymd[0], ymd[1], ymd[2]); 
                PopulateCalendar(x, Math.round(cdt.getTime()/1000) ); 
            }
            else if (ci==0) 
            { 
                mn++; 
                if (mn==12) { yr++;mn=0; }
                var ymd = CalenderMaxDate(x, yr, mn, 1);
                var cdt = new Date(ymd[0], ymd[1], ymd[2]); 
                PopulateCalendar(x, Math.round(cdt.getTime()/1000) ); 
            }
        }
        return ['actionkey', ro, ci, 0]; 
    }

    return ['notfound',0,0,0];  // control not found
}

function DownKey(panelObj, x, etype, tabNavigationControl)
{
    //DeviceConsole('DownKey ['+etype+'], x: '+x+', tabNavigationControl: '+tabNavigationControl);
    var ans = ['updateactiveindex', 0, 0];  // state [1:processed, 0:Not processed], ro
                       
    var ro = Int(panelObj.getAttribute('ro'));
    var ci = Int(panelObj.getAttribute('ci'));
    //var romax = Int($(panelObj).attr('romax'));

    if (etype=='button' || etype=='textbox') 
    {
        if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0];
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='dropdown') 
    {
        var owlObj = panelObj.children[1];
        if (owlObj.style.display=='none') 
        {
            if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj);  return ['updateactiveindex', 0, 0, 0];  }
        }
        else 
        {
            var ronew = GetVisibleItem2(owlObj, ro, 1);
            if (ronew < LASTITEM) {
                FocusToControl__('remove', panelObj);
                panelObj.setAttribute('ro', ronew);
                FocusToControl__('set', panelObj);
            }
        }
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='search') {
        return ['updateactiveindex', ro, ci, 0];
    }
    else if (etype=='nextprevchannel') 
    {
        if (panelObj.getAttribute('keys')==1) { MoveToChannel(-1); return ['actionkey', ro, ci, 0]; }
        return ['updateactiveindex', ro, ci, 0];
    }
    else if (etype=='optionsList') 
    {
        if (Int(panelObj.getAttribute('type'))==0)
        {
            FocusToControl__('remove', panelObj);
            var nextid = x + '_' + (ro+1) + '_' + ci;  
            if (document.getElementById(nextid)==null) {
                if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0]; 
                ro=0;  
            }
            else ro++;
            return ['actionkey', ro, ci, 0]; 
        }
        return ['updateactiveindex', ro, ci, 0]; 
    }
    else if (etype=='resumeplay' || etype=='connectionlist' || etype=='serverlist' || etype=='postergallery' || etype=='channelgallery') 
    {
        var t = Int(panelObj.getAttribute('type'));
        if (t==0)
        {
            var owlObj = document.getElementById('owl'+x);
            var ronew = GetVisibleItem2(owlObj, ro, 1);
            if (ronew >= LASTITEM)
            {
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj);  return ['updateactiveindex', 0, 0, 0]; }
                if ($(panelObj).attr('loop')==0) return ['actioncomplete', 0, 0, 0]; 
                ronew = GetVisibleItem2(owlObj, -1, 1);
            }
            FocusToControl__('remove', panelObj);
            panelObj.setAttribute('ro', ronew);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        else if (t==1 && tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
    }
    else if (etype=='gridlist') 
    {
        var owlObj = document.getElementById('owl'+x);
        var child0 = owlObj.children[0];
        var cmax = Math.floor($(panelObj).width() / $(child0).outerWidth());
        var ro_max = owlObj.children.length;
        if (cmax==1) {
            var newro = GetVisibleItemR(x, ro, '0', 1);
            if (newro>=ro_max) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                newro = GetVisibleItemR(x, -1, '0', 1);
            }
        }
        else {
            var newro = GetVisibleItemR(x, ro+cmax-1, '0', 1);
            if (newro>=ro_max) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                newro = GetVisibleItemR(x, ro_max, '0', -1);
            }
        }
        FocusToControl__('remove', panelObj);
        // set new column
        panelObj.setAttribute('ro', newro);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='rowlist')  
    {
        var ro_max = panelObj.children[0].children.length;  //Int(panelObj.getAttribute('romax'));
        //DeviceConsole(['Downkey - rowlist',ro,romax]);
        var xro = 'R' + x + '_';
        var newro = GetVisibleItem(xro, ro+1, 1, ro_max);
        if (newro==ro_max) 
        { 
            if (tabNavigationControl!=0) {
                Rowlist_Focus(panelObj, 'remove');
                return ['updateactiveindex', ro, ci, 0];
            }
            newro = GetVisibleItem(xro, 0, 1, ro_max);
        }

        Rowlist_RowChanged(panelObj, x, ro, ci, newro);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='episodePanel') 
    {
        var owlObj = document.getElementById('owl'+x);
        var cimax = owlObj.children.length;
        var ro_max = Math.floor( $(owlObj).width() / $(owlObj.children[0]).outerWidth(true) );
        if (ci+ro_max > cimax) { 
            if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0]; 
            ci = ci % ro_max;  
        }
        else ci += ro_max;

        FocusToControl__('remove', panelObj);
        panelObj.setAttribute('ci', ci);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='pguide') 
    { 
        var romax = Int(panelObj.getAttribute('romax'));
        var r = ro;
        //var c = ci+1;
        var tms1 = PGSlot2Time(x)
        SetScroll2time(x, 'time:'+tms1);

        ProgramGuideCell(x, 'remove'); 
        for (r++ ; r < romax ; r++) {
            if (document.getElementById('pg' + x + '_' + r).style.display != 'none') break;
        } 

        if (r > romax) { 
            if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0]; 
            for (r = 1 ; r < ro ; r++) {
                if (document.getElementById('pg' + x + '_' + r).style.display != 'none') break;
            } 
        }

        panelObj.setAttribute('ro', r);
        ProgramGuide_ScrollRowIntoView(x);

        ci = PGTime2Slot(x, r, tms1 );

        panelObj.setAttribute('ci', ci);
        ProgramGuideCell(x, 'set'); 

        return ['actioncomplete',0,0,0]; 
    }
    else if (etype=='keyboard') 
    {
        var romax = panelObj.children.length-1;
        if (ro < romax-1) {
            ro++; 
            var cimax = document.getElementById('R'+x+'_'+ro).children.length - Int(document.getElementById('R'+x+'_'+ro).getAttribute('hiddenkeys'));
            if (ci >= cimax) ci = cimax-1;
        }
        return ['actionkey', ro, ci, 0]; 
    }
    else if (etype=='calendar') 
    {
        if (ro==-1) {
            var yr = Int(panelObj.getAttribute('year'));
            var mn = Int(panelObj.getAttribute('month'));
            if (ci==1) 
            { 
                yr--; 
                var ymd = CalenderMinDate(x, yr, mn, 1);
                var cdt = new Date(ymd[0], ymd[1], ymd[2]); 
                PopulateCalendar(x, Math.round(cdt.getTime()/1000) ); 
            }
            else if (ci==0) 
            {
                mn--; 
                if (mn==-1) { yr--; mn=11; }
                var ymd = CalenderMinDate(x, yr, mn, 1);
                var cdt = new Date(ymd[0], ymd[1], ymd[2]); 
                PopulateCalendar(x, Math.round(cdt.getTime()/1000) ); 
            }
        }
        else {
            if (ro < 5) ro++; 
            var calObj = document.getElementById(x+'_'+ro+'_'+ci);      
            if (calObj != null && Int(calObj.innerHTML)==0) ro--;
        }
        return ['actionkey', ro, ci, 0]; 
    }
    return ['notfound',0,0,0];  // control not found
}

function LeftKey(panelObj, x, etype, tabNavigationControl)
{
    //DeviceConsole('LeftKey '+ etype);
    var ans = ['updateactiveindex', 0, 0];  // state [1:processed, 0:Not processed], ro
                       
    var ro = Int(panelObj.getAttribute('ro'));
    var ci = Int(panelObj.getAttribute('ci'));
    //var romax = Int(panelObj.getAttribute('romax'));

    if (etype=='button' || etype=='textbox')
    {
        if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0];
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='search') return ['updateactiveindex', ro, ci, 0];
    else if (etype=='nextprevchannel') 
    {
        if (panelObj.getAttribute('keys')==0) { MoveToChannel(-1); return ['actionkey', ro, ci, 0]; }
        return ['updateactiveindex', ro, ci, 0];
    }
    else if (etype=='optionsList') 
    {
        if (Int(panelObj.getAttribute('type'))==1)
        {
            var ronew = GetVisibleItemR(x, ro, ci, -1);
            if (ronew==-1) { 
                if (tabNavigationControl!=0) {
                    FocusToControl__('remove', panelObj);
                    return ['updateactiveindex', ro, ci, 0]; 
                }
                return ['actioncomplete', 0, 0, 0]; 
            }
            FocusToControl__('remove', panelObj);
            panelObj.setAttribute('ro', ronew);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        return ['updateactiveindex', ro, ci, 0]; 
    }
    else if (etype=='resumeplay' || etype=='connectionlist' || etype=='serverlist' || etype=='postergallery' || etype=='channelgallery') 
    {
        var t = Int(panelObj.getAttribute('type'));
        if (t==1) {
            var ronew = GetVisibleItem2(document.getElementById('owl'+x), ro, -1);
            if (ronew==-1) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                if ($(panelObj).attr('loop')==0) return ['actioncomplete', 0, 0, 0]; 
                ronew = GetVisibleItem2(owlObj, owlObj.children.length, -1);
            }
            FocusToControl__('remove', panelObj);
            panelObj.setAttribute('ro', ronew);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        else if (t==0 && tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
    }
    else if (etype=='gridlist') 
    {
        var owlObj = document.getElementById('owl'+x);
        var child0 = owlObj.children[0];
        var cmax = Math.floor($(panelObj).width() / $(child0).outerWidth());
        var ro_max = owlObj.children.length;
        if (cmax > 1) {
            var newro = GetVisibleItemR(x, ro, '0', -1);
            if (newro==-1) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                newro = GetVisibleItemR(x, ro_max, '0', -1);
            }
            FocusToControl__('remove', panelObj);
            // set new column
            panelObj.setAttribute('ro', newro);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        else if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='rowlist') 
    {
        var rowObj = document.getElementById('R'+x+'_'+ro);   
        var cimax = Int(rowObj.getAttribute('last'));

        var xro = x+'_'+ro+'_';
        var newci = GetVisibleItem(xro, ci-1, -1, cimax);
        if (newci==-1) 
        { 
            if (tabNavigationControl!=0) { Rowlist_Focus(panelObj, 'remove');  return ['updateactiveindex', ro, ci, 0]; }
            newci = GetVisibleItem(xro, cimax-1, -1, cimax);
        }
        FocusToControl__('remove', panelObj);

        // set new column
        panelObj.setAttribute('ci', newci);
        rowObj.setAttribute('c', newci);

        Rowlist_ScrollColumnIntoView(panelObj, x, ro, newci);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='episodePanel') 
    {
        var owlObj = document.getElementById('owl'+x);
        var cimax = owlObj.children.length;
        ci--;
        if (ci<0) { ci=cimax-1; if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0]; }

        FocusToControl__('remove', panelObj);
        panelObj.setAttribute('ci', ci);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='keyboard')
    {
        var cimax = document.getElementById('R'+x+'_'+ro).children.length - Int(document.getElementById('R'+x+'_'+ro).getAttribute('hiddenkeys'));
        ci--;
        if (ci < 0) ci=cimax-1; 
        return ['actionkey', ro, ci, 0];
    }
    else if (etype=='pguide')
    {
        //var cimax = Int(panelObj.getAttribute('cimax'));
        ProgramGuideCell(x, 'remove'); 
        ci = PGuideGetVisibleCell(x, -1, -1);
        if (ci == -1) 
        { 
            GetPGuideData(x,-1); 
            SetScroll2time(x, 'time:84600'); 
            panelObj.setAttribute('ci', '9999');
            return ['actioncomplete', 0, 0, 0]; 
        }
        panelObj.setAttribute('ci', ci);
        PopulateProgramGuide1(x);
        return ['actioncomplete', ro, ci, 0]; 
    }
    else if (etype=='calendar') { 
        ci--;
        if (ro==-1 && ci < 0) ci=0; 
        else if (ro==0 && Int($('#'+x+'_'+ro+'_'+ci).html())==0) { ro=-1; ci=1; }
        else if (ci < 0) { if (ro==-1) ci=0;  else { ro--;  ci=6; } }
        return ['actionkey', ro, ci, 0]; 
    }

    return ['notfound',0,0,0];  // control not found
}

function RightKey(panelObj, x, etype, tabNavigationControl)
{
    //DeviceConsole('RightKey '+ etype);
    var ans = ['updateactiveindex', 0, 0];  // state [1:processed, 0:Not processed], ro
                       
    var ro = Int(panelObj.getAttribute('ro'));
    var ci = Int(panelObj.getAttribute('ci'));
    //var romax = Int(panelObj.getAttribute('romax'));

    if (etype=='button' || etype=='textbox')
    {
        if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0];
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='search') return ['updateactiveindex', ro, ci, 0];
    else if (etype=='nextprevchannel') 
    {
        if (panelObj.getAttribute('keys')==0) { MoveToChannel(1); return ['actionkey', ro, ci, 0]; }
        return ['updateactiveindex', ro, ci, 0];
    }
    else if (etype=='optionsList') 
    {
        if (Int(panelObj.getAttribute('type'))==1)
        {
            var ronew = GetVisibleItemR(x, ro, ci, 1);
            if (ronew==9999999) { 
                if (tabNavigationControl!=0) {
                    FocusToControl__('remove', panelObj);
                    return ['updateactiveindex', ro, ci, 0]; 
                }
                return ['actioncomplete', 0, 0, 0]; 
            }
            FocusToControl__('remove', panelObj);
            panelObj.setAttribute('ro', ronew);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        return ['updateactiveindex', ro, ci, 0]; 
    }
    else if (etype=='resumeplay' || etype=='connectionlist' || etype=='serverlist' || etype=='postergallery' || etype=='channelgallery') 
    {
        var t = Int(panelObj.getAttribute('type'));
        if (t==1)
        {
            var ronew = GetVisibleItem2(document.getElementById('owl'+x), ro, 1);
            if (ronew==LASTITEM) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                if ($(panelObj).attr('loop')==0) return ['actioncomplete', 0, 0, 0]; 
                ronew = GetVisibleItem2(owlObj, -1, 1);
            }
            FocusToControl__('remove', panelObj);
            panelObj.setAttribute('ro', ronew);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        else if (t==0 && tabNavigationControl!=0) { FocusToControl__('remove', panelObj);  return ['updateactiveindex', 0, 0, 0]; }
    }
    else if (etype=='gridlist') 
    {
        var owlObj = document.getElementById('owl'+x);
        var child0 = owlObj.children[0];
        var cmax = Math.floor($(panelObj).width() / $(child0).outerWidth());
        var ro_max = owlObj.children.length;
        //DeviceConsole([cmax, romax]);
        if (cmax > 1) {
            var newro = GetVisibleItemR(x, ro, '0', 1);
            //DeviceConsole(newro);
            if (newro>=ro_max) { 
                if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
                newro = GetVisibleItemR(x, -1, '0', 1);
            }
            FocusToControl__('remove', panelObj);
            // set new column
            panelObj.setAttribute('ro', newro);
            FocusToControl__('set', panelObj);
            return ['actioncomplete', 0, 0, 0]; 
        }
        else if (tabNavigationControl!=0) { FocusToControl__('remove', panelObj); return ['updateactiveindex', 0, 0, 0]; }
    }
    else if (etype=='rowlist') 
    {
        var rowObj = document.getElementById('R'+x+'_'+ro);
        var cimax = Int(rowObj.getAttribute('last'));

        var xro = x+'_'+ro+'_';
        var newci = GetVisibleItem(xro, ci+1, 1, cimax);
        if (newci==cimax) 
        { 
            if (tabNavigationControl!=0) { Rowlist_Focus(panelObj, 'remove');  return ['updateactiveindex', ro, ci, 0]; }
            newci = GetVisibleItem(xro, 0, 1, cimax);
        }
        FocusToControl__('remove', panelObj);

        // set new column
        panelObj.setAttribute('ci', newci);
        rowObj.setAttribute('c', newci);

        Rowlist_ScrollColumnIntoView(panelObj, x, ro, newci);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='episodePanel') 
    {
        var cimax = document.getElementById('owl'+x).children.length;
        ci++;
        if (ci >= cimax) { ci=0; if (tabNavigationControl!=0) return ['updateactiveindex', ro, ci, 0]; }

        FocusToControl__('remove', panelObj);
        panelObj.setAttribute('ci', ci);
        FocusToControl__('set', panelObj);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='pguide')
    {
        ProgramGuideCell(x, 'remove'); 
        var c0 = document.getElementById(x+'_'+ ro +'_0');
        var cimax = Int(c0.getAttribute('cimax'));
        ci = PGuideGetVisibleCell(x, 1, cimax);
        if (ci == cimax) { 
            FocusToControl__('remove', panelObj); 
            GetPGuideData(x,1); 
            SetScroll2time(x, 'time:0'); ci=0; 
            return ['actioncomplete', 0, 0, 0]; 
        }
        //ci++; 
        panelObj.setAttribute('ci', ci);
        PopulateProgramGuide1(x);
        return ['actioncomplete', 0, 0, 0]; 
    }
    else if (etype=='keyboard')
    {
        var cimax = document.getElementById('R'+x+'_'+ro).children.length - Int(document.getElementById('R'+x+'_'+ro).getAttribute('hiddenkeys'));
        ci++;
        if (ci >=cimax) ci=0; 
        return ['actionkey', ro, ci, 0];
    }
    else if (etype=='calendar') { 
        ci++;
        if (ro==-1) {
            if (ci==2) { ro=0; for(ci=0 ; ci<6 ; ci++) if (Int($('#'+x+'_'+ro+'_'+ci).html()) > 0) break; }
        }
        else { 
            if (ci==7) { ro++; ci=0; if (Int($('#'+x+'_'+ro+'_'+ci).html())==0) { ci=6; ro--; } }
            else if (Int($('#'+x+'_'+ro+'_'+ci).html())==0) ci--; 
        }
        return ['actionkey', ro, ci, 0]; 
    }

    return ['notfound',0,0,0];  // control not found
}

function OKKey(panelObj, x, etype, tabNavigationControl)
{
    DeviceConsole('OKKey');
    var ans = ['updateactiveindex', 0, 0];  // state [1:processed, 0:Not processed], ro
                       
    var ro = Int(panelObj.getAttribute('ro'));
    var ci = Int(panelObj.getAttribute('ci'));

    if (etype=='nextprevchannel') return ['updateactiveindex', ro, ci, 0];
    else if (etype=='button' || etype=='textbox' || etype=='search') 
    {
        var fnname = panelObj.id + '_clickEvent';
        if (etype=='search') fnname = 'searchInput' + x + '_clickEvent';
        if (typeof(window[fnname])=='function') window[fnname]();       

        DeviceConsole('OKKey ---------------');
        return ['actionkey',ro,ci,0];
    }
    else if (etype=='dropdown') 
    {
        var dropdown = panelObj.children[0];
        var owlObj = panelObj.children[1];
        if (owlObj.style.display=='none') { $(owlObj).show();  FocusToControl__('set', panelObj); }
        else 
        {
            $(owlObj).hide(); 
            var itemObj = owlObj.children[ro];
            dropdown.children[0].innerHTML = itemObj.innerHTML;
            panelObj.setAttribute('sel', ro);
            var fnname1 = 'ddChanged_'+x;
            if (typeof(window[fnname1])=='function') window[fnname1](x);       
        }
        return ['actioncomplete',0,0,0];
    }
    else if (etype=='serverlist' || etype=='connectionlist' || etype=='episodePanel' || /*etype=='collist' || */etype=='channelgallery') 
    {
        GetActionUrl(x + '_' + ro + '_' + ci, 'active');
        return ['actionkey',ro,ci,0];
    }
    else if (etype=='resumeplay') 
    {
        $(panelObj).hide();
        $('#screen_container'+screenIndex).attr('activeIndex', GetLastIndex()); 
        WebtvVideoPlay(ro); 
        return ['actionkey',ro,ci,0];
    }
    else if (etype=='gridlist' || etype=='rowlist') 
    {
        if (tabNavigationControl==0) { 

            var fmethod = ($(panelObj).attr('newscreen')=='1') ? 'visible' : 'active';
            GetActionUrl(x + '_' + ro + '_' + ci, fmethod);  
            Device_PlaySound('click');  
            return ['actioncomplete',0,0,0];
        }
        return ['updateactiveindex', 0, 0];
    }
    else if (etype=='pguide') 
    {
        var actionUrl = '';
        var channelsTimeTable = document.getElementById('epgData'+x).innerHTML.split('~');

        var t = panelObj.getAttribute('a');
        //DeviceConsole('OKKey-pguide-t: '+t);
        if (t != '') 
        {
            var parts = t.split('_');
            var k =Int( document.getElementById(parts[0]+'_'+parts[1]+'_0').getAttribute('r') );
            var row = channelsTimeTable[k].split('|');
            //DeviceConsole(row);

            var s = Int(document.getElementById(t).getAttribute('s'));
            var min = Math.floor((s % 3600) / 60);
            var date = new Date( Int(panelObj.getAttribute('timestamp')) * 1000 );
            var tStamp = '' + (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + Math.floor(s/3600) + ':' + min + ':00^';

            var durationInSec = Int(document.getElementById(t).getAttribute('d'));  
            durationInSec += 300;
            tStamp += durationInSec;

            var rCount = row.length;   // assuming 48 slots ,rCount=54
            actionUrl = row[rCount-1] + '&vid=' + row[rCount-5] + '&tz='+tStamp + '&parent='+x;
            if ($(panelObj).attr('pageHasVideo')=='1') actionUrl = '&ctype=epg&id=' + row[rCount-5] + '&tz='+tStamp + '&parent='+x;
        }

        //DeviceConsole('OKKey-pguide-actionUrl: '+actionUrl);
        if (actionUrl=='')
        { 
            var roid = x+'_'+ ro +'_0';
            var rox =Int(document.getElementById(roid).getAttribute('r'));
            var row = channelsTimeTable[rox].split('|');
            //DeviceConsole(row);

            var hr = Math.floor( ci / 2);
            var min = (ci % 2) * 30;
            var date = new Date( Int(panelObj.getAttribute('timestamp')) * 1000 );
            var tStamp = '' + (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + hr + ':' + min + ':00';
            var rCount = row.length;   // assuming 48 slots ,rCount=54
            actionUrl = row[rCount-1] + '&vid=' + row[rCount-5] + '&tz='+ tStamp + '&parent='+ x;
            if (panelObj.getAttribute('pageHasVideo')=='1') actionUrl = '&ctype=epg&id=' + row[rCount-5] + '&tz='+tStamp + '&parent='+x;
        }
        //DeviceConsole('pguide OK: '+actionUrl);
        if ($(panelObj).attr('pageHasVideo')=='1') WebtvVideo(actionUrl);
        else WebTVDetails(actionUrl, ''); 

        return ['actionkey',rox,ci,0];
    }
    else if (etype=='optionsList') 
    {
        $(panelObj).hide();
        $('#screen_container'+screenIndex).attr('activeIndex', ci); 
        GetActionUrl(x + '_' + ro + '_0', 'visible'); 
        return ['actionkey',ro,ci,0];
    }
    else if (etype=='keyboard') { KbKeyPressed(x, ro, ci); return ['actionkey',ro,ci,0]; }
    else if (etype=='calendar') 
    {
        if (ro==-1) 
        {
            panelObj.setAttribute('ci','1');
            return ['overridekey','right',0,0];
        }

        $(panelObj).hide();
        var calObj = document.getElementById(x+'_'+ro+'_'+ci);      
        var cdt = new Date(Int(panelObj.getAttribute('year')), Int(panelObj.getAttribute('month')), Int(calObj.innerHTML));
        var unixTimestamp = Math.round(cdt.getTime()/1000);

        var newObj = GetAttachedToObject(panelObj);
        if (newObj == null) newObj = GetInputElementByIndex(-1);

        var iscontrol = panelObj.getAttribute('iscontrol');
        if (iscontrol==1) SetLastIndex( newObj.getAttribute('keyindex') );

        SetActiveElementByIndex( GetLastIndex() );
        if (newObj.getAttribute('etype')=='pguide') GetPGuideData(newObj.getAttribute('x'),0,unixTimestamp);
        return ['actionkey',ro,ci,0];
    }

    return ['notfound',0,0,0];  // control not found
}

function BackKey(panelObj, x, etype, tabNavigationControl)
{
    DeviceConsole('BackKey: ' + etype);
    var ro = Int(panelObj.getAttribute('ro'));
    var ci = Int(panelObj.getAttribute('ci'));

    if (etype=='button' || etype=='textbox' || etype=='nextprevchannel' || etype=='calendar') 
    {
        GoBack();  
        return ['actioncomplete',0,0,0];
    }
    else if (etype=='gridlist' || etype=='rowlist' || etype=='serverlist' || etype=='search' ||
             etype=='connectionlist' || etype=='pguide' || etype=='episodePanel' || etype=='optionsList' || etype=='postergallery' || etype=='channelgallery') 
    {
        if (tabNavigationControl==0) { GoBack(); return ['actioncomplete',0,0,0]; }
        return ['updateactiveindex',ro,ci,0];
    }
    else if (etype=='dropdown') 
    {
        var dropdown = panelObj.children[0];
        var owlObj = panelObj.children[1];
        if (owlObj.style.display=='none') { GoBack(); }
        else 
        {
            FocusToControl__('remove', panelObj);
            ro = Int(panelObj.getAttribute('sel'));
            panelObj.setAttribute('ro', ro);

            $(owlObj).hide(); 
            var itemObj = owlObj.children[ro];
            dropdown.children[0].innerHTML = itemObj.innerHTML;
            var fnname1 = 'ddChanged_'+x;
            if (typeof(window[fnname1])=='function') window[fnname1](x);       
        }
        return ['actioncomplete',0,0,0];
    }
    else if (etype=='resumeplay') 
    {
        $(panelObj).hide();
        $('#screen_container'+screenIndex).attr('activeIndex', GetLastIndex()); 
        return ['actioncomplete',0,0,0];
    }

    return ['notfound',ro,ci,0];  // control not found
}

function HotKey(panelObj, keycode)
{
    //DeviceConsole('HotKey :'+keycode);
    //if (panelObj != null) DeviceConsole(panelObj);
    for (var i = 0 ; i < 10 ; i++) {
        var obj = GetInputElementByEType('options', i, screenIndex);
        if (obj == null) break;

        var hkey = obj.getAttribute('hotkey');
        //DeviceConsole('Hotkey: '+hkey+', '+keycode);
        if (hkey != keycode) continue;

        var act = obj.getAttribute('action').split(';');
        //DeviceConsole('Hotkey: '+hkey+', action: '+act[0]);

        if (act[0]=='optionslist') ShowOptionsList();
        else if (act[0]=='control') {
            if (act[1].substr(0,9)=='calender~') ShowOptionControl('calendar');  
            if (act[1].substr(0,7)=='search~') ShowOptionControl('search');  
        }
        else if (act[0]=='screen') ShowOptionsList();
        else if (act[0]=='home') GotoHomeScreen();
        else if (act[0]=='logout') logout();
        else if (act[0]=='prevchannel') MoveToChannel(-1);
        else if (act[0]=='nextchannel') MoveToChannel(1);
        else if (act[0]=='javascript') 
        {
            var fname2 = act[1];
            //DeviceConsole('HotKey -> ' + fname2 + ' -> ' + typeof(window[fname2]));
            if (typeof(window[fname2])=='function') window[fname2]();       
        }
        return 'actioncomplete';
    }
    if (keycode=='home') { GotoHomeScreen(); return 'actioncomplete'; }
    return 'notfound';
}

function OnKeyDownEvent__(panelObj, keycode, ignoreTabGroup)
{
    //DeviceConsole('OnKeyDownEvent__: '+keycode+', '+ignoreTabGroup);
    if (panelObj==null) return 13;

    var x = panelObj.getAttribute('x');
    var etype = panelObj.getAttribute('etype');
    var next = panelObj.getAttribute('next');
    var prev = panelObj.getAttribute('prev');
    var controlChanged=0; 

    DeviceConsole('OnKeyDownEvent__: '+keycode+', etype: '+etype);
    var tabNavigationControl = NextPrev(next,prev,keycode);
    keycode = KeyCodeOverride(panelObj, keycode);
    if (HotKey(panelObj, keycode)=='actioncomplete') return 13; 

    var ans = ['notfound',0,0,0];
    var loop = 1;
    while (loop == 1)
    {
        loop = 0;
        if (keycode=='up') ans = UpKey(panelObj, x, etype, tabNavigationControl);
        else if (keycode=='down') ans = DownKey(panelObj, x, etype, tabNavigationControl);
        else if (keycode=='left') ans = LeftKey(panelObj, x, etype, tabNavigationControl);
        else if (keycode=='right') ans = RightKey(panelObj, x, etype, tabNavigationControl);
        else if (keycode=='back') ans = BackKey(panelObj, x, etype, tabNavigationControl);
        else if (keycode=='OK') ans = OKKey(panelObj, x, etype, tabNavigationControl);
        if (ans[0]=='overridekey') { keycode = ans[1]; loop=1; }
    }

    //DeviceConsole('etype: '+etype+','+keycode+'Key ans: '+ans[0]+', ro: '+ans[1]+', ci: '+ans[2]+', data: '+ans[3]+', ignoreTabGroup: '+ignoreTabGroup);
    if (ans[0]=='actionkey') 
    { 
        if (keycode=='OK') { Device_PlaySound('click');  return 13; }
        else if (keycode=='back') return 13;
        else if (keycode=='options') return 13;

        Device_PlaySound('slide');  
        FocusToControl__('remove', panelObj);
        UpdateSelectedIndexOfPanel(panelObj, ans[1], ans[2]);

        if (etype=='pguide')
        {
            if (ans[3]==1) $('#epgData'+x).attr('populated', 0);
            if (ans[2]==9999) return 13;
        }
        FocusToControl__('set', panelObj);
        return 13;
    }
    else if (ans[0]=='actioncomplete') return 0;
    else if (ans[0]=='updateactiveindex') 
    {
        if (ignoreTabGroup==0) 
        {
            var tg = Int( $(panelObj).attr('tabgroup') );
            //DeviceConsole('tabgroup: '+tg);
            if (tg > 0) 
            {
                var elist = GetElementsBy_('tabgroup', tg);
                //DeviceConsole('elist length: '+elist.length);
                for (var i = 0 ; i < elist.length ; i++) 
                {
                    if (elist[i]==panelObj) continue;
                    if (elist[i].style.display=='none') continue;

                    var ans = OnKeyDownEvent__(elist[i], keycode, 1);
                    if (ans==13) return 13;
                }
            }
        }

        if (etype=='button' || etype=='textbox' || etype=='search') FocusToControl__('remove', panelObj);
        //FocusFunction(panelObj, 'remove');
        UpdateActiveIndex(tabNavigationControl, panelObj);
        return 0;
    }
    else if (ans[0]=='notfound' && keycode=='back') 
    {
        GoBack();        
        return 0;
    }

    //DeviceConsole('**** OnKeyDownEvent__ -> ' + etype + ', ppanel' + x + ', keycode ->' + keycode);
/*
    if (etype=='gridlist' || etype=='rowlist') 
    {
        var ro = Int(panelObj.getAttribute('ro'));
        var romax = panelObj.children[0].children.length;
        //var romax = Int(panelObj.getAttribute('romax'));
        if (romax==0) return 13;

        var ci = Int(panelObj.getAttribute('ci'));
        var cimax = Int(panelObj.getAttribute('cimax'));

        var rowlistOffset = { 'ro':ro, 'offset':0, 'ci':ci  };
        if (keycode=='OK') { 
            controlChanged = NextPrev(next,prev,keycode); 
            if (controlChanged==0) { GetActionUrl(x + '_' + ro + '_' + ci, 'active'); return 13; } 
        }
        // update values  
        if (controlChanged==0) { Device_PlaySound('slide'); UpdateSelectedIndexOfPanel_(panelObj, ro, ci); return 13; }

        var ans = UpdateActiveIndex(controlChanged, panelObj);
        //return 'updateactiveindex';
        if (ans==13) return 13;
        if (ans==0) return 0; 
        return 13; 
    }*/
    return 0;
}

function SetFocus()
{
    FocusToControl('set');
}

function FindParentHavingAttribute(me, parentAttribute)
{
    var obj=me;
    while(me.id != 'fcon') {
        if (obj.hasAttribute(parentAttribute)) return obj;
        obj = obj.parentElement;
    }
    return null;
}

function GetScreenIndex(scObj)
{
    var obj = FindParentHavingAttribute(scObj, 'activeIndex');
    if (obj==null) return 0;
    return obj.id.substring(16);
    //return Int(obj.id.substring(16));
}

function OnMouseDown(e)
{
    e.stopPropagation();
    var targetID = e.target.id;
    DeviceConsole('OnMouseDown targetID: ' + targetID);
    DeviceConsole(e);

    if (e.target.id=='') 
    {
        var obj=e.target;
        var pObj = $(obj).closest('.dragClass');
        if (pObj != null) targetID=pObj.attr('id');
    }
    var obj = document.getElementById(targetID);
    if (obj==null) return false;

    DeviceConsole('OnMouseDown targetID: ' + targetID);
    DeviceConsole('OnMouseDown etype: ' + obj.getAttribute('etype'));

    if (e.type=='mousedown') {
        obj.setAttribute('dragPos', e.screenX+','+e.screenY);
        myClick = [0, e.screenX, e.screenY, targetID];
    }
    else if (e.type=='touchstart') {
        obj.setAttribute('dragPos', e.targetTouches[0].clientX+','+e.targetTouches[0].clientY);
        myClick = [0, e.targetTouches[0].clientX, e.targetTouches[0].clientY, targetID];
    }
}

function OnMouseMove(e)
{
    if (e.type=='mousemove' && e.buttons != 1) return;
    e.stopPropagation();
    DeviceConsole('OnMouseMove : ' + myClick[3] + ', e.target.id: ' + e.target.id);

    var obj = document.getElementById(myClick[3]);
    if (obj==null) return false;
    var etypeObj = $(obj).closest('[etype]')[0];
    var etype = etypeObj.getAttribute('etype');
    //DeviceConsole(etype);

    cpos = obj.getAttribute('dragPos').split(',');

    //DeviceConsole(e.target);
    var dragX = obj.getAttribute('dragX');
    if (dragX != '') {
        var obj1 = obj;
        var offX = 0;
        if (e.type=='mousemove') offX = e.screenX-Int(cpos[0]);
        else if (e.type=='touchmove') offX = e.targetTouches[0].clientX-Int(cpos[0]);

        if (dragX != 'self') obj1 = document.getElementById(dragX);
        if (obj1 != null)
        { 

            if (appVersion > 120 && etype=='rowlist') {
                var mlX = obj.parentNode.scrollLeft;
                var xx = mlX - offX;
                obj.parentNode.scrollTo(xx, 0);
            }
            else {
                var mleft = Int(obj1.style.marginLeft);
                mleft += offX;
                if (mleft > 0) mleft = 0;
                if (obj1.offsetWidth+mleft < obj1.parentElement.offsetWidth) mleft = obj1.parentElement.offsetWidth - obj1.offsetWidth;
                obj1.style.marginLeft = mleft + 'px'; 
            }
        }
    }
    var dragY = obj.getAttribute('dragY');
    if (dragY != '') {
        var obj1 = obj;
        var offY = 0;

        if (e.type=='mousemove') offY = e.screenY-Int(cpos[1]);
        else if (e.type=='touchmove') offY = e.targetTouches[0].clientY-Int(cpos[1]);

        if (dragY != 'self') obj1 = document.getElementById(dragY);
        if (obj1 != null) {
            var mleft = Int(obj1.style.marginTop);
            mleft += offY;
            if (mleft > 0) mleft = 0;
            if (obj1.offsetHeight+mleft < obj1.parentElement.offsetHeight) mleft = obj1.parentElement.offsetHeight - obj1.offsetHeight;
            obj1.style.marginTop = mleft + 'px'; 
        }
    }

    if (e.type=='mousemove') obj.setAttribute('dragPos', e.screenX+','+e.screenY);
    else if (e.type=='touchmove') obj.setAttribute('dragPos', e.targetTouches[0].clientX+','+e.targetTouches[0].clientY);
}

function OnMouseUp(e)
{
    var diffX = 0;
    var diffY = 0;
    DeviceConsole('OnMouseUp: ' + myClick[3]);
    if (e.type=='mouseup') {
        diffX = e.screenX - myClick[1];
        diffY = e.screenY - myClick[2];
    }
    else if (e.type=='touchend') {
        var obj = document.getElementById(myClick[3]);
        if (obj==null) return false;

        cpos = obj.getAttribute('dragPos').split(',');

        diffX = Int(cpos[0]) - myClick[1];
        diffY = Int(cpos[1]) - myClick[2];
    }

    if (diffX < -2 || diffX > 2) myClick[0]=1;
    if (diffY < -2 || diffY > 2) myClick[0]=1;

    if (myClick[0]==1) {
        myClick.push(e.target.id);
        DeviceConsole(myClick);
        if (myClick[3].substr(0,3)=='owl') {
            var ids = myClick[3].substr(3).split('_');
            var obj = document.getElementById('ppanel'+ids[0]);
            var etype = $(obj).attr('etype');
            if (etype=='rowlist'/* || etype=='collist'*/) Rowlist_LoadImages(ids[0]);
        }
    }
}

function Rowlist_LoadRowImage(x,r,c)
{
    //DeviceConsole('Rowlist_LoadRowImage(' + x + ',' + r + ',' + c + ')');
    var obj = document.getElementById(x+'_'+r+'_'+c);
    if (obj==null) return 0;
    if (obj.style.display=='none') return -1;
    if (obj.getAttribute('loaded') == null) {
        for (var k = 0 ; k < obj.children.length ; k++) {
            if (obj.children[k].tagName != 'IMG') continue;
            var src = obj.children[k].getAttribute('imgSource');
            if (src != '') { obj.children[k].src = src; $(obj.children[k]).attr('imgSource', ''); }
        }
        obj.setAttribute('loaded','1');
        return 1;
    }
    return 2;
}

function Rowlist_LoadRowImages(x,r)
{
    //DeviceConsole('Rowlist_LoadRowImages: '+x+', r: '+r);

    var panelObj = document.getElementById('ppanel'+x);
    var psz = [ $(panelObj).offset().left, $(panelObj).width(), $(panelObj).offset().top, $(panelObj).height()];

    var owlObj = document.getElementById('owl' + x + '_' + r);
    var owlsz = [ $(owlObj).offset().left, $(owlObj).width(), $(owlObj).offset().top, $(owlObj).height()];

    var itemWidth = $('#' + x + '_' + r + '_0').outerWidth();

    var c0 = Math.floor( Math.abs(psz[0]-owlsz[0]) / itemWidth );
    if (c0 < 0) c0=0;
    var visibleOnScreen = Math.ceil( Math.abs(psz[1]) / itemWidth );
    var contentCount = owlObj.children.length;

    //DeviceConsole(psz);
    //DeviceConsole(owlsz);
    //DeviceConsole('Rowlist_LoadRowImages >  contentCount: '+contentCount+', visibleOnScreen: '+visibleOnScreen);

    for (var i = 0 ; i < contentCount && visibleOnScreen > 0; i++)
        if (Rowlist_LoadRowImage(x, r, c0+i) > 0) visibleOnScreen--;
}

function Rowlist_LoadImages(x)
{
    DeviceConsole('Rowlist_LoadImages : '+x);
    var panelObj = document.getElementById('ppanel'+x);
    var psz = [ $(panelObj).offset().left, $(panelObj).width(), $(panelObj).offset().top, $(panelObj).height()];
    var romax = panelObj.getAttribute('romax');
    for (var r=0 ; r<romax ; r++) {
        var r0 = 'R' + x + '_' + r;
        var r0Obj = document.getElementById(r0);
        if (r0Obj==null) break;
        var rsz = [ $(r0Obj).offset().left, $(r0Obj).width(), $(r0Obj).offset().top, $(r0Obj).height()];

        if (rsz[2]+rsz[3] < psz[2]) continue;
        //if (rsz[2] > psz[2]+psz[3]) break;
        Rowlist_LoadRowImages(x, r);
    }
}

function PGuideMouseWheelAction(e)
{
    var evt = window.event || e;
    var delta=0;
    if (evt.detail) delta = evt.detail * -120;
    else delta = evt.wheelDelta;

    var pObj = $(evt.target).closest('[x]');
    if (pObj == null) return;

    var romax = Int(pObj.attr('romax'));
    var newRotop = rotop + ( (delta < 0) ? 1 : -1);

    var scrollPercentage = newRotop * 100 / romax;
    if (scrollPercentage >= 0) OnScrollArrowClicked(scrollPercentage, 'down', xAttr);
}

function PGuideAttachMouseWheelEvent(x)
{
    var panelObj = document.getElementById('ppanel'+x);
    if (panelObj == null) return;

    var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
    if (panelObj.attachEvent) panelObj.attachEvent("on"+mousewheelevt, PGuideMouseWheelAction);
    else if (panelObj.addEventListener) panelObj.addEventListener(mousewheelevt, PGuideMouseWheelAction, false);
}

function OnScrollArrowClicked(tableid, side, x)
{
    var panelObj = document.getElementById('ppanel'+x);
    var etype = panelObj.getAttribute('etype');
    DeviceConsole('OnScrollArrowClicked(tableid: '+tableid+', side: '+side+', x: '+x+', etype: '+etype+')');
    if (etype=='rowlist')
    {
        var obj = document.getElementById(tableid);
        if (obj==null) return false;

        var offX = $(obj).parent().width(); 
        if (side=='right') offX = -offX;

        var mleft = Int(obj.style.marginLeft);
        mleft += offX;
        if (mleft > 0) mleft = 0;
        if (obj.offsetWidth+mleft < obj.parentElement.offsetWidth) mleft = obj.parentElement.offsetWidth - obj.offsetWidth;
        obj.style.marginLeft = mleft + 'px'; 

        var xr = tableid.split('_');
        var psz = [ $(panelObj).offset().left, $(panelObj).width(), $(panelObj).offset().top, $(panelObj).height()];
        Rowlist_LoadRowImages(x, xr[1]);
    }
    else if (etype=='pguide')
    {
        var ro = Int(panelObj.getAttribute('ro'));
        var romax = Int(panelObj.getAttribute('romax'));

        var ci = Int(panelObj.getAttribute('ci'));
        var rowsToScroll = Math.round(romax * tableid / 100);
        ro = rowsToScroll; 
        //if (rotop + roscreen > romax) { rotop = romax - roscreen; ro = rotop; }

        FocusToControl__('remove', panelObj);
        UpdateSelectedIndexOfPanel(panelObj, ro, ci);
        FocusToControl__('set', panelObj);
    }
}

//==============================================================================
function PanelClick(e)
{
    e.stopPropagation();
    var targetID = e.target.id;
    if (e.target.id=='') {
        var pObj = $(e.target).closest('[etype]');
        var etype = pObj.attr('etype');
        //DeviceConsole(etype + ', ' + e.target.innerHTML);
        if (etype=='rowlist') {
            if (e.target.innerHTML=='&gt;' || e.target.innerHTML=='&lt;') { 
                FocusToControl__('remove', pObj[0]);                

                var rowObj = $(e.target).closest('[last]');
                var parts = rowObj.attr('id').split('_');
                pObj.attr('ro', parts[1]);
                pObj.attr('ci', rowObj.attr('c'));

                if (e.target.innerHTML=='&gt;') OnKeyDownEvent__(pObj[0], 'right', 1);  
                if (e.target.innerHTML=='&lt;') OnKeyDownEvent__(pObj[0], 'left', 1);  
                return; 
            }
        }

        var obj=e.target;
        while(obj.id != 'fcon') {
            if (obj.hasAttribute('ctype')) { OnFocusEvent(null,obj); return true; }
            obj = obj.parentElement;
        }
        return null;
    }
    else {
        var parts = e.target.id.split('_');
        while(parts.length < 2) parts.push('');

        if (parts[1]=='up') {
            var panelObj = document.getElementById(parts[0]);
            OnKeyDownEvent__(panelObj, 'up', 1);
        }
        else if (parts[1]=='dn') {
            var panelObj = document.getElementById(parts[0]);
            OnKeyDownEvent__(panelObj, 'down', 1);
        }
    }
}

function ClearSearch(me)
{
    var panelObj = null;
    if (me == undefined) panelObj = GetInputElementByEType('search',0);
    else panelObj = $(me).closest('[etype]')[0];  

    if (panelObj.style.display != 'none') 
    {
        var x = panelObj.getAttribute('x');
        $('#searchInput'+x).val('');
        ApplySearchFilter(document.getElementById('searchInput'+x));
    }    
    return 1;
}

function ApplySearchFilter(me)
{
    var pObj = $(me).closest('[etype]')
    //DeviceConsole('ApplySearchFilter: '+pObj.attr('busy'));
    if (pObj.attr('busy')=='0') 
    {
        var prevvalue = me.getAttribute('prevvalue');
        if (prevvalue != me.value) {
            me.setAttribute('prevvalue', me.value);
            pObj.attr('busy', '1');  // timer set but not fired yet
            window.setTimeout(function() { ApplySearchFilter_(me.id); }, 400); 
        }
    }
}

function OnInputChanged(e)
{
    ApplySearchFilter(e.target);
}

function ApplySearchFilter_(meid)
{
    //DeviceConsole('ApplySearchFilter_: '+meid);
    var me = document.getElementById(meid);
    var panelObj = me.parentNode.parentNode;
    var applyOn = panelObj.getAttribute('attachedto');
    var searchMethod = panelObj.getAttribute('smethod');
    //DeviceConsole([applyOn, searchMethod]);

    var ai = applyOn.split('~');

    var csi = screenIndex;
    screenIndex = GetScreenIndex(me);  // me.parentNode.parentNode.getAttribute('screenindex');
    newObj = GetInputElementByEType(ai[0], Int(ai[1]));  
    screenIndex = csi;

    if (newObj==null) {
        panelObj.setAttribute('busy', '0');
        return 0;
    }

    var searchFor = new RegExp(me.value, "i");
    var etype = newObj.getAttribute('etype');

    if (searchMethod=='1')
    {
        systemSearch(searchFor, applyOn);
        return;
    }
    //DeviceConsole([ai[0], ai[1], newObj, searchFor, etype]);

    if (etype=='rowlist') 
    {
        var firstVisible = [-1,-1]; 
        var romax = Int(newObj.getAttribute('romax'));
        var x = newObj.getAttribute('x');
        var classToSelect = '.p'+x+'_text';

        $('#owl'+x+'_0').css('marginLeft', '0px');
        for (var i = 0 ; i < romax ; i++)
        {
            var last = Int($('#R'+x+'_'+i).attr('last'));
            var visibleCount=0;   
            for (var j = 0 ; j < last ; j++)
            {
                var obj = $('#'+x+'_'+i+'_'+j);
                var label = $('#'+x+'_'+i+'_'+j+' > div:first').html();

                //var label = obj.find(classToSelect).html();

                //var label = obj.attr('label');
                if (label.search(searchFor)==-1) obj.hide();
                else { 
                    obj.show(); visibleCount++; 
                    if (firstVisible[0]==-1) { firstVisible[0] = i;  firstVisible[1] = j; } 
                }
            }

            if (visibleCount==0) $('#R'+x+'_'+i).hide();
            else {
                $('#R'+x+'_'+i).show();
                $('#'+x+'_'+i+'_counter').html('1/'+visibleCount);
            }
        }
        Rowlist_LoadImages(x);
        newObj.setAttribute('ro', firstVisible[0]);
        newObj.setAttribute('ci', firstVisible[1]);
        $('#rowGroup'+x).css('marginTop', '0px');
    }
    else if (etype=='pguide') 
    {
        var x = newObj.getAttribute('x');
        var romax = newObj.getAttribute('romax');

        $('#epgData'+x).attr('populated', 0);
        newObj.setAttribute('searchfilter', me.value);
        $('#'+x+'_pguide').css('marginTop', '0px');
        var tms1 = PGSlot2Time(x)
        PopulateProgramGuide(x);

        SetScroll2time(x, 'time:'+tms1);
        for (var r = 1 ; r <= romax ; r++) {
            if (document.getElementById('pg' + x + '_' + r).style.display != 'none') break;
        }
        ci = PGTime2Slot(x, r, tms1 );

        newObj.setAttribute('ro', r);
        newObj.setAttribute('ci', ci);
        ProgramGuide_ScrollRowIntoView(x);
        ProgramGuideCell(x, 'set'); 
    }
    panelObj.setAttribute('busy', '0');
}

function SearchFilterToggle_(obj, action)
{
    var x = obj.getAttribute('x');
    var iscontrol = obj.getAttribute('iscontrol');
    var visibleonfocus = obj.getAttribute('visibleonfocus');
    //DeviceConsole('SearchFilterToggle_ ' + iscontrol + ', ' + visibleonfocus);

    if (visibleonfocus!=1) return 0;
    if (iscontrol==1)
    {
        var d = document.getElementById('searchBox'+x).style.display;
        if (action=='set' && d=='none') { $('#searchBox'+x).show('slide', 'right');  $('#searchBox' + x + ' input:first-child').focus(); }
        else if (action=='remove' && d!='none') $('#searchBox'+x).hide('slide', 'right');
        else {
            if (d=='none') { $('#searchBox'+x).show('slide', 'right'); $('#searchBox' + x + ' input:first-child').focus(); }
            else $('#searchBox'+x).hide('slide', 'right');
        }
    }
    else
    {
        var d = obj.style.display;
        if (d=='none' && action=='set') { obj.style.display='block'; $('#searchBox'+x+' input:first-child').focus();  }
        else if (d!='none' && action=='remove') obj.style.display='none';
    }
}

function SearchFilterToggle(me)
{
    SearchFilterToggle_(me.parentNode);
}

function SearchFilterClose(me)
{
    OnKeyDownEvent__(me.parentNode, 'OK', 1);
}

function AutoPlayVideo(obj)
{   
    var x = obj.getAttribute('x');
    var ro = $(obj).attr('ro');
    var owlObj = document.getElementById('owl'+x);
    var itemObj = owlObj.children[ro];
    var gmt = Int(itemObj.getAttribute('gmt'));

    var onload = obj.getAttribute('onload');
    if (onload=='0') { PopulateProgramList(gmt);  return; }
    if (itemObj.getAttribute('ctype')!='video') { PopulateProgramList(gmt);  return; }

    //DeviceConsole('AutoPlayVideo: ' + itemid + ', appVersion: '+appVersion+', onload: '+onload);
    //var parts = itemid.split('_');
    var panelObj = document.getElementById('ppanel'+x);

    if (onload==3 && appVersion < 112) {
        onload=1;

        // if there are more than one timezones, set onload to 0
        var itemObj2 = owlObj.children[0];  // document.getElementById(x+'_1_0');
        if (itemObj2 != null && itemObj2.getAttribute('ctype')=='video') { obj.setAttribute('onload', '0'); return; }
    }

    //DeviceConsole('AutoPlayVideo: onload=' + onload);
    if (appVersion < 118 && IsVideoPlaying()==1) CloseVideo(''); 
    if (onload==1 || onload==2) 
    {
        document.getElementById('fcon').setAttribute('videoData', '^^^^^^^^^^^^^^^^');
        obj.setAttribute('onload', '0');
    }

    var sa = obj.getAttribute('sa');
    DeviceConsole('AutoPlayVideo: onload=' + onload+', sa: '+sa);
    if (sa==0) WebtvVideoX('video', panelObj, itemObj.getAttribute('tz'));
    else obj.setAttribute('sa', '0');

    window.setTimeout("PopulateProgramList(" + gmt + ")", 1500);
}

function systemSearchResult(msg)
{
    var data = JSON.parse(msg);
    //DeviceConsole(msg);

    newObj = document.getElementById('ppanel'+data.x);
    if (newObj==null) return 0;
    newObj.setAttribute('romax', data.cimax+1);
    newObj.setAttribute('ro', '0');
    document.getElementById('owl'+data.x).innerHTML = data.result;
    FocusToControl('set');

    var searchObj = GetInputElementByEType('search', 0);  
    if (searchObj != null) {
        searchObj.setAttribute('busy', '0');
        if (data.cimax==-1) 
        {
            SetActiveElementByIndex(searchObj.getAttribute('keyindex'));
            var inputTextObj = document.getElementById('searchInput'+searchObj.getAttribute('x'));
            if (inputTextObj.value.length < 3) inputTextObj.value = '';
        }
    }
}

function systemSearch(searchFor, applyOn)
{
    //DeviceConsole('systemSearch: ['+searchFor+']');
    var ai = applyOn.split('~');
    //DeviceConsole('systemSearch: '+ai);

    newObj = GetInputElementByEType(ai[0], Int(ai[1]));  
    if (newObj==null) return 0;

    var pObj = $(newObj).closest('[recid]');
    AjaxCall('&action=search&txt='+searchFor + '&x=' + newObj.getAttribute('x') + '&recid=' + pObj.attr('recid') + '&etype=' + newObj.getAttribute('etype') + GetPotionK(), systemSearchResult, '', DeviceConsole);   
}

function ChannelGallery_Play(x, ro2play, playStream)
{
    var panelObj = document.getElementById('ppanel'+x);
    var ro = $(panelObj).attr('ro');
    var itemidActual = x + '_' + ro + '_' + ci;
    if (ro != ro2play) return;

    var owlObj = document.getElementById('owl'+x);
    var obj = owlObj.children[ro];

    var vd = $(panelObj).attr('video');

    if (vd==1) {
        var vidParts = $('#fcon').attr('videodata').split('^^'); 
        var tz = 0;
        if (vidParts.length >= 6) tz = vidParts[5];

        var parent = document.getElementById('vcontainer'+x);
        var sz = obj.getBoundingClientRect();
        parent.style.left = ((sz.x==undefined) ? sz.left : sz.x) + 'px';
        parent.style.top = ((sz.y==undefined) ? sz.top  : sz.y) + 'px';
        parent.style.width = ($(parent).width() + 5) + 'px';
        parent.style.height = ($(parent).height() + 5) + 'px';

        if (playStream==1) {   
            var param0 = '&ctype=video&id='+ obj.getAttribute('cid') + '&tz='+((tz==0) ? obj.getAttribute('tz') : tz); 
            var streamData = AjaxCallWaitForResponse('&action=getStreamUrl'+param0 + GetPotionK(), '').split('^^'); 
            Device_VideoSetPlayer(streamData[0], '', 0, x, 'channelgallery');
        }
    }
    else if (vd==2) {
        var parent = document.getElementById('poster_'+x);
        $(parent).show();

        if (playStream==1) {   
            var param0 = '&ctype=video&id='+ obj.getAttribute('cid') + '&tz='+((tz==0) ? obj.getAttribute('tz') : tz); 
            var streamData = AjaxCallWaitForResponse('&action=getStreamUrl'+param0 + GetPotionK(), '').split('^^'); 
            Device_VideoSetPlayer(streamData[0], '', 0, x, 'channelgallery');
        }
    }
}

function APopulateDropdown(x, NameValueString, selected)
{
    var panelObj = document.getElementById('ppanel'+x);
    var dropBox = panelObj.children[1];
    var dropDown = panelObj.children[0];
    var ctype = panelObj.getAttribute('ctype');

    var nvs = NameValueString.split(';');
    var out = '';
    var ro = 0;
    var selText = nvs[0]; 
    var k = 0;
    for (var i = 0 ; i < nvs.length ; i+=2, k++)
    {
        if (selected == nvs[i+1]) { ro=k;  selText=nvs[i]; }
        //out += "<div id='"+x+"_"+k+"_0' cid='"+nvs[i+1]+"' ctype='"+ctype+"'>" + nvs[i] + "</div>";
        out += "<div cid='"+nvs[i+1]+"'>" + nvs[i] + "</div>";
    }
    panelObj.setAttribute('ro', ro);
    dropBox.innerHTML = out;
    dropDown.children[0].innerHTML = selText;
}

// line no 2356
